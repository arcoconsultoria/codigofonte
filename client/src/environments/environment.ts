// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiSigaUrl: 'http://54.39.202.243/webrunstudio/',
  apiSigaKey: '2AF5B586-3955-4D7E-937F-D72545F21466',
  apiCicloUrl: 'https://appapiweb.azurewebsites.net/api/v1/',
  userApiCiclo: 'api.siga@cicloinsight.com.br',
  passwordApiCiclo: '$ig@+Ap1+@rc02022',
  //rest API
  apiSigaParts:"https://dev.ivendas.digital:8443/webrunstudio/",
  secretKeyGlobal: "14E3016F-65A9-4B3B-A57A-0FAC7CABEF1CDEADD186-7392-4740-848B-38595213EC4C80D0E260-A48A-4A44-963F-21A44E05CD1E"
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
