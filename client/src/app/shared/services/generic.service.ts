import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injector, Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { SessionService } from './session.service';
import { DataTransferObject } from '../models/data-transfer-object.model';
import { AuthenticationCicloService } from 'src/app/core/authentication/authentication-ciclo.service';

@Injectable()
export abstract class GenericService<T extends DataTransferObject> {

  protected baseUrl;
  protected http: HttpClient;
  protected apiPath: string;
  protected sessionServer: SessionService;
  protected authenticationCicloService: AuthenticationCicloService

  constructor(
    protected injector: Injector,
    baseUrl: string
  ) {
    this.http = injector.get(HttpClient);
    this.baseUrl = baseUrl;
    this.sessionServer = injector.get(SessionService);
    this.authenticationCicloService = injector.get(AuthenticationCicloService);
  }

  get(url: string): Observable<T> {
    let options = { headers: this.getHeaders(), withCredentials: true };
    return this.http.get<T>(url, options);
  }

  loadAll(): Observable<Array<T>> {
    let options = { headers: this.getHeaders(), withCredentials: true };
    return this.http.get<Array<T>>(this.getFullUrl(), options);
  }

  serach(codigo: string): Observable<T> {
    let options = { headers: this.getHeaders(), withCredentials: true };
    let url = `${this.getFullUrl()}/${codigo}`;
    return this.http.get<T>(url, options);
  } 

  post<T>(dto: T): Observable<T> {
    let options = { headers: this.getHeaders() };
    const urlFull = `${this.apiPath}`;
    return this.http.post<T>(this.getFullUrl(), dto, options);
  }

  protected getParameterizedFullUrl(url: string, parameter: string) {
    return this.baseUrl + url + '/' + this.clear(parameter); //TODO mais roigog ubstez
  }

  clear(parameter: string) {
    return parameter.replace(/\//g, "%2F"); //TODO deve ter um jeito melhor de fazer, talvez com HttpUrlEncodingCodec
  }

  protected getFullUrl() {
    return `${this.baseUrl}${this.apiPath}`;
  }

  protected getHeaders(): HttpHeaders {
    return new HttpHeaders()
      .set('Content-Type', 'application/json; charset=utf-8')
  }

}
