import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { sha256 } from 'js-sha256';
import { Observable, throwError } from 'rxjs';
import { SessionService } from './session.service';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class RestService {
  private ws;
  private login:string="";

  constructor(private  http:  HttpClient,
              private session: SessionService

              ) {
        //get rest URL
        this.ws = environment.apiSigaParts;

  };


  soNumero(value){
    return value == null ? null : value.replace(/[^\d]+/g,'');
  }

  private getHash(texto:string){
    return sha256.hmac(this.session.getSecretKey(), texto);
  }

  getControlAccess(login: string, senha: string){
    return {
        "chave": this.getHash(login+senha),
        "origem": "Web"
      };

   }

   getSecretKey(login: string){
    let observable = new Observable(observer => {
        let postData = {
          'login': login
        };
        let data = this.http.post(this.ws + "wsIdentificadorEmpresa.rule?sys=API", JSON.stringify(postData))
          .subscribe(
            data => {
              let response = JSON.parse(JSON.stringify(data));
              //check error
              if(response.status =='error'){
                observer.error(response.data);
               }
               //set dados identificador da empresa
              let Secret_Key=response.data[0].secretKey;
              this.session.setSecretKey(Secret_Key);
              observer.next(Secret_Key);
            },
            error =>{
              observer.error(error);
            }
            );

        });
    return observable;
}

   autenticate(login: string,senha: string): any{
        this.login = login;
        let postData = {
        'login': login,
        "controlAccess": this.getControlAccess(login,senha)
        };
        return this.http.post(this.ws + "wsLogin.rule?sys=API", JSON.stringify(postData))

    }

    validaToken(token: string){
      let postData = {
        'login': this.login,
        "controlAccess": {
          "token": token,
          "origem": "Web"
          }
      };
      return this.http.post(this.ws + "wsValidarToken.rule?sys=API", JSON.stringify(postData));
    }

    //request token "esqueci a senha?"
    SolicitarTokenSenha(login: string): any{
      this.login = login;
      let postData = {
      'login': login
      };
      return this.http.post(this.ws + "wsSolicitarTokenSenha.rule?sys=API", JSON.stringify(postData))

  }

  //request token "esqueci a senha?"
  ConfirmarNovaSenha(token: string, senha:string): any{
        let login = this.login;
        let postData = {
          "login": login,
          "controlAccess": {
              "chave": this.getHash(login+senha),
              "token": token,
              "origem": "Web"
            }
          }
        return this.http.post(this.ws + "wsConfirmarNovaSenha.rule?sys=API", JSON.stringify(postData))

    }

  getStates(): any{
      let postData = {}
       return this.http.post(this.ws + "wsBuscarUF.rule?sys=API", JSON.stringify(postData));
  }

  getCitys(idUF): any{
    let postData = {"idUF":idUF}
     return this.http.post(this.ws + "wsBuscarCidades.rule?sys=API", JSON.stringify(postData));
}

}
