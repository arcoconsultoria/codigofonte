import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { UserSession } from '../models/user-session.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SessionService {

  private readonly keyCurrentSession = 'orcamentacao-current-session';
  private sessionSubject = new Subject<UserSession>();
  public isLogado = new Subject<boolean>();

  setCurrentSession(sessionDto: UserSession): void {
    localStorage.setItem(this.keyCurrentSession, JSON.stringify(sessionDto));
    this.sessionSubject.next(sessionDto);
    /*if(sessionDto.logado){
      this.isLogado.next(true);
    }else{
      this.isLogado.next(false);
    }*/
  }

  setCurrentSinistro(idSinistro: number): void {
    let userSession = this.getUserSession();
    //userSession.idSinistro = idSinistro;
    this.setCurrentSession(userSession);
  }

  setCurrentTokenCiclo(token: string) {
    let userSession = this.getUserSession();
    userSession.TokenCiclo = token;
    this.setCurrentSession(userSession);
  }

  setCurrentToken(token: string) {
    let userSession = this.getUserSession();
    userSession.token = token;
    this.setCurrentSession(userSession);
  }

  getCurrentToken() {
    return this.getUserSession().token;
  }

  setSecretKey(secret: string) {
    let userSession = this.getUserSession();
    userSession.secretKey = secret;
    this.setCurrentSession(userSession);
  }

  getSecretKey() {
    return this.getUserSession().secretKey;
  }

  setEmail(value:string){
    localStorage.setItem(this.keyCurrentSession+"_email",value);
  }

  getEmail(){
    const value = localStorage.getItem(this.keyCurrentSession+"_email");
    if(value){
      return value;
    }
    return "";
  }


  getUserSession(): UserSession {
    let userSession: UserSession;
    if (this.hasCurrentSession()) {
      userSession = this.getCurrentSession();
    } else {
      userSession = new UserSession();
    }
    //check isLogado
    if(userSession.logado){
      this.isLogado.next(true);
    }else{
      this.isLogado.next(false);
    }

    return userSession;
  }

  hasCurrentSession(): boolean {
    return localStorage.getItem(this.keyCurrentSession) !== null;
  }

  getCurrentSession(): UserSession {
    return JSON.parse(localStorage.getItem(this.keyCurrentSession));
  }

  clear() {
    localStorage.removeItem(this.keyCurrentSession);
    this.sessionSubject.next(null);
    //this.isLogado.next(false);
  }

  getSessionSubject() {
    return this.sessionSubject;
  }

  isAuthenticated(){
    return this.getUserSession().logado;
  }

}
