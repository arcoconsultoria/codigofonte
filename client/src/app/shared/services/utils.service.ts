import { Injectable } from '@angular/core';
import { AbstractControl } from '@angular/forms';
import * as moment from 'moment';

@Injectable()
export class Utils{

  static roundCurrency(value: number): number {
    let decimals = 2;
    return this.roundNumber(value, decimals);
  }

  static roundNumber(value: number, decimals: number): number {
    return Number( Math.round( parseFloat(value+'e'+decimals) ) + 'e-' + decimals );
  }

  static containsError(...controls: Array<AbstractControl>): boolean {
    let contains = false;
    if (controls){
      for (var i = 0; i < controls.length; i++) {
        if ( controls[i].errors ){
          contains = true;
          break;
        }
      }
    }
    return contains;
  }

  static stringToDate(strDate: any): Date {
    return new Date(moment(strDate).format())
  }

  static addMonthsDate(date: Date, months: number): Date {
    return moment(date).add(months, 'M').toDate();
  }

  static addDays(date: Date, number: number): Date {
    return moment(date).add(number, 'd').toDate();
  }
}

