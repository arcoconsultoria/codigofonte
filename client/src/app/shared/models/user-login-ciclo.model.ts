import { DataTransferObject } from "./data-transfer-object.model";

export class UserLoginCiclo extends DataTransferObject {
  userName: string;
  password: string;
}
