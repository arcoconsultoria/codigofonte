export class RefreshToken {
  oldToken: string;
  access_token: string;
}
