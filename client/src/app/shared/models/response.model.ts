export class Response {
  message: string;

  hasException(): boolean {
    return this.message !== undefined || this.message !== '';
  }

}
