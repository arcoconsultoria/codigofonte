export class EntityReference {
  id: number = 0;
  presentation: string = '';
  version: number = 0;
}
