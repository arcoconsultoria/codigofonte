import { Response } from "./response.model";

export class Search extends Response {
  searchEntry: string;
  searchTotalPages: number;
  searchNumberPage: number;
}
