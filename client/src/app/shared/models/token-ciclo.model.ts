import { DataTransferObject } from "./data-transfer-object.model";

export class TokenCiclo extends DataTransferObject {
  usuario: string;
  token: string;
}
