import { CurrencyPipe } from '@angular/common';
import { Component, forwardRef, Injector, Input  } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { GenericInputFieldComponent } from '../generic-input-field/generic-input-field.component';

@Component({
  selector: 'app-input-field-currency',
  templateUrl: './input-field-currency.component.html',
  styleUrls: ['./input-field-currency.component.css'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => InputFieldCurrencyComponent),
    multi: true
  }]
})
export class InputFieldCurrencyComponent extends GenericInputFieldComponent {

  @Input() isCurrency: boolean = false;

  constructor(injector: Injector, private currencyPipe: CurrencyPipe) {
    super(injector);
   } 

}
