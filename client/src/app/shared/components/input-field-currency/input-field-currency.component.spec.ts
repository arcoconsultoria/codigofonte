import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InputFieldCurrencyComponent } from './input-field-currency.component';

describe('InputFieldCurrencyComponent', () => {
  let component: InputFieldCurrencyComponent;
  let fixture: ComponentFixture<InputFieldCurrencyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InputFieldCurrencyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InputFieldCurrencyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
