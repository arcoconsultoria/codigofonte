import { Component, forwardRef, Injector } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { GenericInputFieldComponent } from '../generic-input-field/generic-input-field.component';

@Component({
  selector: 'app-input-field-checkbox',
  templateUrl: './input-field-checkbox.component.html',
  styleUrls: ['./input-field-checkbox.component.css'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => InputFieldCheckboxComponent),
    multi: true
  }]
})
export class InputFieldCheckboxComponent extends GenericInputFieldComponent {

  constructor(injector: Injector) {
    super(injector);
  }


}
