import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InputFieldCheckboxComponent } from './input-field-checkbox.component';

describe('InputFieldCheckboxComponent', () => {
  let component: InputFieldCheckboxComponent;
  let fixture: ComponentFixture<InputFieldCheckboxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InputFieldCheckboxComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InputFieldCheckboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
