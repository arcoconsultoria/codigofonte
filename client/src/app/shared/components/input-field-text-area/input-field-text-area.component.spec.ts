import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InputFieldTextAreaComponent } from './input-field-text-area.component';

describe('InputFieldTextAreaComponent', () => {
  let component: InputFieldTextAreaComponent;
  let fixture: ComponentFixture<InputFieldTextAreaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InputFieldTextAreaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InputFieldTextAreaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
