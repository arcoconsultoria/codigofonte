import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InputFieldHourComponent } from './input-field-hour.component';

describe('InputFieldHourComponent', () => {
  let component: InputFieldHourComponent;
  let fixture: ComponentFixture<InputFieldHourComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InputFieldHourComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InputFieldHourComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
