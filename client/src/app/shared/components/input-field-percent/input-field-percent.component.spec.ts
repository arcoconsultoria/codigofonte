import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InputFieldPercentComponent } from './input-field-percent.component';

describe('InputFieldPercentComponent', () => {
  let component: InputFieldPercentComponent;
  let fixture: ComponentFixture<InputFieldPercentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InputFieldPercentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InputFieldPercentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
