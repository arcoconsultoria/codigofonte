import { OnInit, Directive, Injector } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { Entity } from '../../models/entity.model';
import { GenericService } from '../../services/generic.service';
import { Search } from '../../models/search.model';
import { environment } from '../../../../environments/environment';


@Directive()
export abstract class GenericListComponent<T extends Entity> implements OnInit {

  public curreentPageNumber: number;
  public resources: Array<T> = new Array<T>();
  public showSpinner: boolean = false;
  public resourceForm: FormGroup;
  public resources$: Observable<Array<T>>;
  public qtdItens: number;

  protected formBuilder: FormBuilder;
  protected hasPagination: boolean;

  constructor(
    protected resourceService: GenericService<T>,
    protected injector: Injector
  ) {
    this.formBuilder = this.injector.get(FormBuilder);
    this.hasPagination = false;
  }

  ngOnInit() {
    this.buildResourceForm();
    this.resetNumberPage();
    this.onSearch();
    this.loadAll(this.curreentPageNumber, '');
  }

  protected abstract buildResourceForm(): void;

  private resetNumberPage(): void {
    this.curreentPageNumber = 1;
  }

  onSearch(): void {
    this.resourceForm.get('searchEntry').valueChanges
      .subscribe(
        (value) => {
          this.resetNumberPage();
          this.loadAll(this.curreentPageNumber, value);
        }
      );
  }

  loadAll(numberPage: number, value: string) {
    this.activateSpinner();
    let collection: Search = new Search();
    collection.searchNumberPage = numberPage;
    collection.searchEntry = value;
    this.resourceService.postSerach(collection)
      .subscribe(
        resources => {
          if (resources) {
            this.resources = resources
          }
        },
        (error) => console.log(error),
        () => this.deactivateSpinner()
      );
  }

  deleteResorce(resource: T) {
    const mustDelete = confirm('Deseja realmente escluir este item ?');
    if (mustDelete) {
      this.resourceService.delete(resource.id).subscribe(
        () => {
          this.resetNumberPage();
          this.loadAll(this.curreentPageNumber, '');
        }
        // () => alert('Erro ao tentar excluir')
      );
    }
  }

  presentationDate(date: Date): string {
    var minDate = moment.utc("0001-01-01");
    if (moment.utc(date).isAfter(minDate))
      return this.getDate(date);
    else
      return '';
  }

  getDate(date: Date): string {
    return moment(date).format('DD/MM/YYYY');
  }

  getDateHor(date: Date): string {
    return moment(date).format('DD/MM/YYYY HH:mm:ss');
  }

  public hasNextPage(): boolean {
    return this.resources.length === environment.searchMaxResults;
  }

  public nextPageLoad(): void {
    this.curreentPageNumber++;
    this.hasPagination = true;
    this.loadAll(this.curreentPageNumber, this.getValueShared());
  }

  public getValueShared(): string {
    return this.resourceForm.controls['searchEntry'].value;
  }

  public hasPreviousPage(): boolean {
    return this.curreentPageNumber > 1;
  }

  public previousPageLoad(): void {
    this.curreentPageNumber--;
    this.hasPagination = true;
    this.loadAll(this.curreentPageNumber, this.getValueShared());
  }

  private activateSpinner() {
    this.showSpinner = true;
  }

  private deactivateSpinner() {
    this.showSpinner = false;
  }

}
