import {
  OnInit,
  AfterContentChecked,
  Injector,
  Directive,
  OnDestroy,
} from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';

import { switchMap } from 'rxjs/operators';
import { AlertService } from '../../../core/alert/shared/alert.service';
import { GenericService } from '../../services/generic.service';
import { Entity } from '../../models/entity.model';
import { EntityReference } from '../../models/entity-reference.model';

@Directive()
export abstract class GenericFormComponent<T extends Entity>
  implements OnInit, OnDestroy, AfterContentChecked {

  readonly searchGroupName: string = 'search';

  currentAction: string;
  resourceForm: FormGroup;
  pageTitle: string;
  serverErrorMessages: string[] = null;
  showSpinner: boolean = false;

  protected route: ActivatedRoute;
  protected router: Router;
  protected formBuilder: FormBuilder;
  protected alertService: AlertService;

  constructor(
    protected injector: Injector,
    public resource: T,
    protected resourceService: GenericService<T>
  ) {
    this.route = this.injector.get(ActivatedRoute);
    this.router = this.injector.get(Router);
    this.formBuilder = this.injector.get(FormBuilder);
    this.alertService = this.injector.get(AlertService);
  }

  ngOnInit() {
    this.setCurrentAction();
    this.buildResourceForm();
    this.loadResource();
  }

  ngOnDestroy() {
    this.resourceService = null;
    this.formBuilder = null;
    this.alertService = null;
  }

  ngAfterContentChecked(): void {
    this.setPageTitle();
  }

  submitForm() {
    //this.submittingForm = true;
    if (this.currentAction === 'new') {
      this.createResource();
    } else {
      this.updateResource();
    }
  }

  protected setCurrentAction() {
    if (this.route.snapshot.url[0].path === 'new') {
      this.currentAction = 'new';
    } else {
      this.currentAction = 'edit';
    }
  }

  isEdition(): boolean {
    return this.currentAction === 'edit';
  }

  getResource(): void {
    if (this.currentAction === 'edit') {
      this.activateSpinner();
      this.route.paramMap
        .pipe(
          switchMap((params) => this.resourceService.get(+params.get('id')))
        )
        .subscribe((resource) => (this.resource = resource));
    }
  }

  protected activateSpinner() {
    this.showSpinner = true;
  }

  protected deactivateSpinner() {
    this.showSpinner = false;
  }

  protected loadResource() {
    if (this.currentAction === 'edit') {
      this.activateSpinner();
      this.route.paramMap
        .pipe(
          switchMap((params) => this.resourceService.get(+params.get('id')))
        )
        .subscribe(
          (resource) => {
            this.resource = resource;
            this.resourceForm.patchValue(this.resource);
          },
          (error: HttpErrorResponse) => this.actionsFromError(error),
          () => this.deactivateSpinner()
        );
      //this.deactivateSpinner();
    }
  }

  protected setPageTitle() {
    if (this.currentAction === 'new') {
      this.pageTitle = this.creationPageTitle();
    } else {
      this.pageTitle = this.editionPageTitle();
    }
  }

  protected createResource() {
    let entity: T = this.resourceForm.value as T;
    this.activateSpinner();
    this.resourceService.post(entity).subscribe(
      (resource) => {
        if (resource.hasException) {
          this.alertService.error(resource.message)
        } else {
          this.resource = resource;
          this.actionsForSuccess(resource)
        }
      },
      (error) => {
        this.actionsFromError(error)
      },
      () => this.deactivateSpinner()
    );
  }

  protected updateResource() {
    let entity: T = this.resourceForm.value as T;
    this.activateSpinner();
    this.resourceService.post(entity).subscribe(
      (resource) => {
        if (resource.hasException) {
          this.alertService.error(resource.message);
        } else {
          this.actionsForSuccess(resource)
        }
      } ,
      (error) => this.actionsFromError(error),
      () => this.deactivateSpinner()
    );
  }

  protected actionsForSuccess(resource: T): void {
    if (!resource.hasException) {
      this.alertService.success(
        `${new Date()} - Solicitação processada com sucesso!`
      );

      const baseComponentPath: string = this.route.snapshot.parent.url[0].path;
      // forçar um recarregamento
      this.router
        .navigateByUrl(baseComponentPath, { skipLocationChange: true })
        .then(() =>
          this.router.navigate([baseComponentPath, resource.id, 'edit'])
        );
    }
  }

  protected actionsFromError(error: HttpErrorResponse): void {
    this.alertService.error('Ocoreu um erro ao processar sua solicitação.'); //TODO
    //this.submittingForm = false;

    if (error.status === 422) {
      //this.alertService.error(JSON.parse(error._body).erros);
      this.alertService.error(error.message);
    } if (error.status === 500){
      this.alertService.error(error.error.message);
    } else {
      let msg: string;
      if (error)
        msg = `Falha na comunicação com o servidor. \n ${error.message}`;
      else
        msg = `Falha na comunicação com o servidor.`;

      this.alertService.error(msg);
    }
  }

  protected getFromGroupEntityReference(): FormGroup {
    return this.formBuilder.group({
      id: [0],
      presentation: [''],
      version: [0],
    });
  }

  protected getFormGroupEntity(): FormGroup {
    return this.formBuilder.group({
      id: [this.resource.id],
      presentation: [this.resource.presentation],
      version: [this.resource.version],
      creationDateTime: [this.resource.createDateTime],
      creationUser: [this.resource.createUser],
      lastUpdateDateTime: [this.resource.lastUpdateDateTime],
      lastUpdateUser: [this.resource.lastUpdateUser],
      deleteMe: [this.resource.deleteMe],
      smartEntry: [this.resource.smartEntry],
      response: this.formBuilder.group({
        exception: [''],
        hasException: [false],
      }),
    });
  }

  protected getGenericFormGroupEntity(): FormGroup {
    return this.formBuilder.group({
      id: [0],
      presentation: [''],
      version: [0],
      creationDateTime: [null],
      creationUser: [''],
      lastUpdateDateTime: [null],
      lastUpdateUser: [''],
      deleteMe: [false],
      smartEntry: [''],
      response: this.formBuilder.group({
        exception: [''],
        hasException: [false],
      }),
    });
  }

  protected abstract buildResourceForm(): void;

  protected abstract creationPageTitle(): string;

  protected abstract editionPageTitle(): string;

  getPresentation(entityReference: EntityReference): string {
    if (entityReference) return entityReference.presentation;
    else '';
  }
}
