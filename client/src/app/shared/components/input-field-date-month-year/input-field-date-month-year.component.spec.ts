import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InputFieldDateMonthYearComponent } from './input-field-date-month-year.component';

describe('InputFieldDateMonthYearComponent', () => {
  let component: InputFieldDateMonthYearComponent;
  let fixture: ComponentFixture<InputFieldDateMonthYearComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InputFieldDateMonthYearComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InputFieldDateMonthYearComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
