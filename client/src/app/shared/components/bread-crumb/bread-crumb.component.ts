import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'bread-crumb',
  templateUrl: './bread-crumb.component.html',
  styleUrls: ['./bread-crumb.component.css']
})
export class BreadCrumbComponent implements OnInit {

  @Input() items: Array<string> = [];

  constructor() { }

  ngOnInit(): void {
  }

  isTheLastItem(item: string): boolean {
    const index = this.items.indexOf(item);
    return index + 1 === this.items.length;
  }

}
