import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectFieldSearchComponent } from './select-field-search.component';

describe('SelectFieldSearchComponent', () => {
  let component: SelectFieldSearchComponent;
  let fixture: ComponentFixture<SelectFieldSearchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SelectFieldSearchComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectFieldSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
