import { Component, Input, forwardRef, Injector, Output, EventEmitter  } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { GenericInputFieldComponent } from '../generic-input-field/generic-input-field.component';

@Component({
  selector: 'app-select-field-search',
  templateUrl: './select-field-search.component.html',
  styleUrls: ['./select-field-search.component.css'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => SelectFieldSearchComponent),
    multi: true
  }]
})
export class SelectFieldSearchComponent extends GenericInputFieldComponent {
  @Input() itens: Array<any>
  @Output() change = new EventEmitter();

  constructor(injector: Injector) {
    super(injector);
   }

   onChange(event){
    this.change.emit(event);
   }
}
