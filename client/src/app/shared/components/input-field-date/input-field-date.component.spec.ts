import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InputFieldDateComponent } from './input-field-date.component';

describe('InputFieldDateComponent', () => {
  let component: InputFieldDateComponent;
  let fixture: ComponentFixture<InputFieldDateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InputFieldDateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InputFieldDateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
