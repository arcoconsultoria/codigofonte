import { Component, OnInit } from '@angular/core';
import { SessionService } from '../services/session.service';
import { Subject } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-sidenavbar',
  templateUrl: './sidenavbar.component.html',
  styleUrls: ['./sidenavbar.component.css']
})
export class SidenavbarComponent {
  public logado: Subject<boolean> = this.session.isLogado;
  constructor(private session: SessionService, private router: Router) { }

  logoff(){
    this.session.clear();
    this.router.navigate(['/login/login-form']);
  }

}
