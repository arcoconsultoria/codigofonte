import { Router } from "@angular/router";
import { Injectable } from "@angular/core";

import { UserSession } from "../../../shared/models/user-session.model";
import { SessionService } from "../../../shared/services/session.service";
import { AuthenticationCicloService } from '../../../core/authentication/authentication-ciclo.service';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(
    private session: SessionService,
    private router: Router,
    private authenticationCicloService: AuthenticationCicloService
    ) { }

  login(idSinistro: number) {
    let userSession: UserSession = new UserSession();
    if (this.session.hasCurrentSession()) {
      this.session.setCurrentSinistro(idSinistro);
      this.router.navigate(['/veiculo'], { queryParams: { sinistro: idSinistro } })
    } else {
      this.authenticationCicloService.getToken()
        .subscribe(token => {
          userSession.token = token;
          userSession.idSinistro = idSinistro;
          this.session.setCurrentSession(userSession);
          this.router.navigate(['/veiculo'], { queryParams: { sinistro: idSinistro } })
        });
    }
  }

}

