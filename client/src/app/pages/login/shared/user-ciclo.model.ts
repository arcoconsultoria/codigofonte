import { Response } from "../../../shared/models/response.model";

export class UserCiclo extends Response {
  userName: string;
  token: string;
}
