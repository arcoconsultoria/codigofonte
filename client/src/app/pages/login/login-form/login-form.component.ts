import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators  } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NavBarComponent } from 'src/app/core/nav-bar/nav-bar.component';
//import { LoginService } from '../shared/login.service';
import { RestService } from '../../../shared/services/rest.service';
import { SessionService } from 'src/app/shared/services/session.service';
import { MessageService} from '../../../shared/message.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {
  displayAlterarSenha = "none";
  email:string;
  password:string;
  Secret_Key:string="";

  formGroup:FormGroup;
  formSubmitAttempt:boolean=false;
  esqueceuSenha:boolean=false;

  //form esqueci senha
  formGroupSenha:FormGroup;
  formSubmitSenha:boolean=false;

  modelSenha:dSenha = new dSenha;

  private idSinistro: number;


  constructor(private route: ActivatedRoute,
              private router: Router,
              //private loginService: LoginService,
              private sessionService: SessionService,
              private formBuilder: FormBuilder,
              private rest:RestService,
              private info:MessageService) {

            //validators
            this.formGroup = this.formBuilder.group({
              email: [null, [Validators.required, Validators.email]],
              password: [null, Validators.required],
            });

            //validator form alterar senha
            this.formGroupSenha = this.formBuilder.group({
            token: [null, [Validators.required]],
            senha: [null, [Validators.required,Validators.minLength(6)]],
            senha_confirma: [null, Validators.required]
            });


     }

  ngOnInit(): void {
     this.email = this.sessionService.getEmail();
  }

  autentication() {
    //this.route.queryParamMap.subscribe(
    /*  params => {
        this.idSinistro = Number(params.get('sinistro'))
      }
    )
    this.loginService.login(this.idSinistro);
    */

  }

  login() {
    this.formSubmitAttempt=true;
    this.esqueceuSenha=false;

    if (!this.formGroup.valid) {
      //console.log("Formulário inválido");
      return;
    }

    //força iniciar nova sessão
    this.sessionService.clear();

    this.rest.getSecretKey(this.email).subscribe(
      Secret_Key => {

              //storage email
              this.sessionService.setEmail(this.email);

               this.rest.autenticate(this.email,this.password).subscribe(
                data => {
                  var response = JSON.parse(JSON.stringify(data));
                  //check error
                  if(response.status =='error'){
                    //this.info.showErro(response.data);
                  }else{
                    let dados = response.data[0];
                    //requer validação?
                    let validarToken=response.data[0].validarToken;
                    //grava sessão
                    let userSession = this.sessionService.getUserSession();
                    userSession.token = dados.controlAccess.transactionHash;
                    userSession.login = dados.login;
                    userSession.razaoSocial = dados.razaoSocial;
                    userSession.cnpjEmpresa = dados.cnpjEmpresa;
                    if(validarToken!='SIM'){
                      userSession.logado = true;
                    }
                    this.sessionService.setCurrentSession(userSession);
                    //Requer validação?
                    if(validarToken=='SIM'){
                      this.validaToken();
                    }else{
                      this.router.navigate(['/home']);
                    }
                  }
                });
              });
  }


  validaToken(){
    Swal.fire({
      buttonsStyling: false,
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      title: 'Segurança',
      html: 'A verificação em duas etapas está <b>habilitada</b>, informe o token enviado para seu email e/ou whatsapp.'+
            '<input type="tel" id="token" class="swal2-input" placeholder="Token" autocomplete="off">',
      confirmButtonText: 'Validar',
      focusConfirm: false,
      preConfirm: () => {
        const token = (<HTMLInputElement>Swal.getPopup().querySelector('#token')).value;
         if (!token) {
          Swal.showValidationMessage('Informe o token recebido')
        }
        return token;
      }
    }).then((result) => {
      if (result.isConfirmed) {
          //check rest validação do token
          let token = result.value;
          this.rest.validaToken(token).subscribe(
            data => {
              var response = JSON.parse(JSON.stringify(data));
              //check error
              if(response.status =='error'){
                this.info.showErro(response.data).then((result) => {
                  this.validaToken();
                });
              }else {
                //atualiza sessão
                let dados = response.data[0];
                let userSession = this.sessionService.getUserSession();
                userSession.controlAccess = dados.controlAccess;
                userSession.logado=true;
                this.sessionService.setCurrentSession(userSession);
                 //to home page
                this.router.navigate(['/home']);
              }

            }
          ,error => {
            this.info.showErro(error.message).then((result) => {
              this.validaToken();
            });
            console.log(error);
            //this.info.showErrorRequest(error);
            }
            );
      }
    })
  }


  esqueciSenha() {
    //força error show error validação email
    this.esqueceuSenha=true;

    if (!this.formGroup.controls.email.valid) {
       return;
    }

    //força iniciar nova sessão
    this.sessionService.clear();

    this.rest.getSecretKey(this.email).subscribe(
      Secret_Key => {
               this.rest.SolicitarTokenSenha(this.email).subscribe(
                data => {
                  if(data.status=="success"){
                    this.openModalAlterarSenha();
                  }
                });
      });
  }

  /*
  showCadastrarNovaSenha(){
    Swal.fire({
      allowOutsideClick: false,
      showCancelButton: true,
      buttonsStyling: false,
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      title: 'Recuperação de acesso',
      html: '<div class="form-nova-senha">Para realizar o cadastro da sua nova senha de acesso informe os dados abaixo:<br/>'+
            //'<div class="info-senha">A senha deve conter: 1. Uma letra maíuscula; 2. Uma letra minúscula; 3. Um número; 4. Um caractere especial; 5. No minimo 6 caracteres</div>'+
            '<label>Token</label><input type="tel" id="token" class="swal2-input" placeholder="Token" autocomplete="off">'+
            '<label>Nova senha</label><input (keydown)="InputSenhaChanged($event)" type="password" id="senha" class="swal2-input" placeholder="Senha" autocomplete="off">'+
            '<label>Confirma senha</label><input type="password" id="senha_confirma" class="swal2-input" placeholder="Senha" autocomplete="off">'+
            "</div>",
      confirmButtonText: 'Gravar',
      cancelButtonText: 'Cancelar',
      focusConfirm: false,
      preConfirm: () => {
        const token = (<HTMLInputElement>Swal.getPopup().querySelector('#token')).value;
        const senha = (<HTMLInputElement>Swal.getPopup().querySelector('#senha')).value;
        const senha_confirma = (<HTMLInputElement>Swal.getPopup().querySelector('#senha_confirma')).value;
         if (!token) {
          Swal.showValidationMessage('Informe o token recebido')
          return;
        }
        if (!senha) {
          Swal.showValidationMessage('Informe a nova senha')
          return;
        }
        if (!senha_confirma) {
          Swal.showValidationMessage('Confirme a nova senha')
          return;
        }
        if (senha!=senha_confirma) {
          Swal.showValidationMessage('A senha e a confirmação não são iguais')
          return;
        }
        //validação senha segura
        var letrasMaiusculas = /[A-Z]/;
        var letrasMinusculas = /[a-z]/;
        var numeros = /[0-9]/;
        var caracteresEspeciais = /[!|@|#|$|%|^|&|*|(|)|-|_]/;

        if(!letrasMaiusculas.test(senha)){
          Swal.showValidationMessage('A senha deve conter uma letra Maiúscula ao menos')
          return;
        }
        if(!letrasMinusculas.test(senha)){
          Swal.showValidationMessage('A senha deve conter uma letra Minúscula ao menos')
          return;
        }
        if(!numeros.test(senha)){
          Swal.showValidationMessage('A senha deve conter um número ao menos')
          return;
        }
        if(!caracteresEspeciais.test(senha)){
          Swal.showValidationMessage('A senha deve conter um caractere especial ao menos')
          return;
        }
        if(senha.length < 6){
          Swal.showValidationMessage('A senha deve conter no minimo 6 caracteres')
          return;
        }

        return {'token':token,'senha':senha,'senha_confirma':senha_confirma};
      }
    }).then((result) => {
      if (result.isConfirmed) {
        alert("aqui")
         /*
          //check rest validação do token
          let token = result.value;
          this.rest.validaToken(token).subscribe(
            data => {
              var response = JSON.parse(JSON.stringify(data));
              //check error
              if(response.status =='error'){
                this.info.showErro(response.data).then((result) => {
                  this.validaToken();
                });
              }else {
                //atualiza sessão
                let dados = response.data[0];
                let userSession = this.sessionService.getUserSession();
                userSession.controlAccess = dados.controlAccess;
                userSession.logado=true;
                this.sessionService.setCurrentSession(userSession);
                 //to home page
                this.router.navigate(['/home']);
              }

            }
          ,error => {
            this.info.showErro(error.message).then((result) => {
              this.validaToken();
            });
            console.log(error);
            //this.info.showErrorRequest(error);
            }
            );

      }
    })
  }*/


  checkValidateSenha(type:number){
    let senha = this.modelSenha.senha;
    var letrasMaiusculas = /[A-Z]/;
    var letrasMinusculas = /[a-z]/;
    var numeros = /[0-9]/;
    var caracteresEspeciais = /[!|@|#|$|%|^|&|*|(|)|-|_]/

    //evita duas mensagens
    if((this.formGroupSenha.controls.senha.touched  || this.formSubmitSenha) && this.formGroupSenha.controls.senha.valid){
      if(type==1){
        return !letrasMaiusculas.test(senha);
      }
      if(type==2){
        return !letrasMinusculas.test(senha);
      }
      if(type==3){
        return !numeros.test(senha);
      }
      if(type==4){
        return !caracteresEspeciais.test(senha);
      }
  }

  }
  setNovaSenha() {
    this.formSubmitSenha=true;
    this.esqueceuSenha=false;

    if (!this.formGroupSenha.valid) {
      return;
    }
    if(this.modelSenha.senha != this.modelSenha.senha_confirma){
      return;
    }

    if(this.checkValidateSenha(1) ||
      this.checkValidateSenha(2) ||
      this.checkValidateSenha(3) ||
      this.checkValidateSenha(4)){
        return ;
      }

         //check rest validação do token
         this.rest.ConfirmarNovaSenha(this.modelSenha.token,this.modelSenha.senha).subscribe(
           data => {
             var response = JSON.parse(JSON.stringify(data));
             //check error
             if(response.status =='success'){
             this.info.showSuccess("Senha alterada com sucesso, realize o login");
             this.onCloseModal();
             }

           });


  }


  openModalAlterarSenha() {
    this.formGroupSenha.reset();
    this.formSubmitSenha=false;
    this.displayAlterarSenha = "block";
    setTimeout(()=>{                           // <<<---using ()=> syntax
      let inputField: HTMLInputElement = document.querySelector('#token');
      inputField.focus();
    }, 100);
  }

  onCloseModal() {
    this.displayAlterarSenha = "none";
  }

}

export class dSenha{
token:string;
senha:string;
senha_confirma:string;
}
