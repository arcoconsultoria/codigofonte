import { Injectable, Injector } from '@angular/core';
import { Observable } from 'rxjs';
import { GenericService } from 'src/app/shared/services/generic.service';
import { Modelo } from '../model/modelo.model';
import { environment } from "../../../../../environments/environment";
import { Modelos } from '../model/modelos.models';



@Injectable({
  providedIn: 'root'
})
export class ModeloService extends GenericService<Modelo> {

  constructor(protected injector: Injector) {
    super(injector, environment.apiCicloUrl)
    this.apiPath = 'geracao'
  }
  getAll(geracao: string): Observable<Modelos> {
    let options = { headers: this.getHeaders(), withCredentials: true };
    let url = this.getFullUrl() + '/' + geracao
    return this.http.get<Modelos>(url, options)
  }
}
