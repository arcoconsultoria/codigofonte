import { Injectable, Injector } from "@angular/core";
import { Observable } from "rxjs";

import { GenericService } from "src/app/shared/services/generic.service";
import { environment } from "src/environments/environment";
import { PecasCiclo } from "../model/pecas-ciclo.model";

@Injectable({
  providedIn: 'root'
})
export class PecaService extends GenericService<PecasCiclo> {

  constructor(protected injector: Injector) {
    super(injector, environment.apiCicloUrl)
    this.apiPath = 'subgrupo';
  }

  serachVeiculo(codigoVeiculo: number, codigoSubGrupo: number): Observable<PecasCiclo> {
    let options = { headers: this.getHeaders(), withCredentials: true };
    let url = `${this.getFullUrl()}/${codigoVeiculo}/${codigoSubGrupo}`;
    return this.http.get<PecasCiclo>(url, options);
  }

}
