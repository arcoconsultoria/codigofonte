import { Injectable, Injector } from "@angular/core";
import { Observable } from "rxjs";

import { GenericService } from "src/app/shared/services/generic.service";
import { environment } from "src/environments/environment";
import { Evento } from "../model/evento.model";

@Injectable({
  providedIn: 'root'
})
export class SigaService extends GenericService<Evento> {

  constructor(protected injector: Injector) {
    super(injector, environment.apiSigaUrl)
    this.apiPath = 'DadosDoEvento.rule';
  }

  getEvento(idEvento: number): Observable<Evento> {
    //let options = { headers: this.getHeadersSiga() };
    let url = `${this.getFullUrl()}?sys=SIG&id=${idEvento}&key=${environment.apiSigaKey}`;
    return this.http.get<Evento>(url);
  }

}
