import { Injectable, Injector } from '@angular/core';
import { Observable } from 'rxjs';
import { GenericService } from 'src/app/shared/services/generic.service';
import { environment } from 'src/environments/environment';
import { Modelos } from '../model/modelos.models';
import { Versao } from '../model/versao.model';
import { Versoes } from '../model/versoes.model';


@Injectable({
  providedIn: 'root'
})
export class VersaoService extends GenericService<Versao> {

  constructor(protected injector: Injector) {
    super(injector, environment.apiCicloUrl)
    this.apiPath = 'modelo'
  }
  getAll(modelo: string): Observable<Versoes> {
    let options = { headers: this.getHeaders(), withCredentials: true };
    let url = this.getFullUrl() + '/' + modelo
    return this.http.get<Versoes>(url, options)
  }
}
