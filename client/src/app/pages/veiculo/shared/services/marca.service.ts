import { Injectable, Injector } from "@angular/core";
import { environment } from "../../../../../environments/environment";

import { GenericService } from "../../../../shared/services/generic.service";
import { MarcaCiclo } from "../model/marca-ciclo.model";

@Injectable({
  providedIn: 'root'
})
export class MarcaService extends GenericService<MarcaCiclo> {

  constructor(protected injector: Injector) {
    super(injector, environment.apiCicloUrl)
    this.apiPath = 'marca'
  }

}
