import { Injectable, Injector } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "../../../../../environments/environment";

import { GenericService } from "../../../../shared/services/generic.service";
import { SessionService } from "../../../../shared/services/session.service";
import { VeiculoCiclo } from "../model/veiculo-ciclo.model";

@Injectable({
  providedIn: 'root'
})
export class VeiculoService extends GenericService<VeiculoCiclo> {

  constructor(protected injector: Injector) {
    super(injector, environment.apiSigaUrl)
    this.apiPath = 'Veiculo'
  }

  getVeiculo(sinistro: string): Observable<VeiculoCiclo> {
    let url = `${this.getFullUrl()}?sinistro=${sinistro}`
    return this.get(url);
  }

  consultaPlaca(placa: string): Observable<VeiculoCiclo> {
    let url = `${environment.apiCicloUrl}Consulta/placa/${placa}`;
    return this.get(url);
  }
}
