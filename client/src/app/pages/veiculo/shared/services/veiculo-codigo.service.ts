import { Injectable, Injector } from '@angular/core';
import { Observable } from 'rxjs';
import { GenericService } from 'src/app/shared/services/generic.service';
import { environment } from 'src/environments/environment';
import { VeiculoCodigo } from '../model/veiculo-codigo.model';
import { VeiculosCodigo } from '../model/veiculos-codigo.model';
import { Versoes } from '../model/versoes.model';



@Injectable({
  providedIn: 'root'
})
export class VeiculoCodigoService extends GenericService<VeiculoCodigo> {

  constructor(protected injector: Injector) {

    super(injector, environment.apiCicloUrl)
    this.apiPath = 'versao'
  }

  getAll(versao: string): Observable<VeiculosCodigo> {
    let options = { headers: this.getHeaders(), withCredentials: true };
    let url = this.getFullUrl() + '/' + versao
    return this.http.get<VeiculosCodigo>(url, options)
  }
}
