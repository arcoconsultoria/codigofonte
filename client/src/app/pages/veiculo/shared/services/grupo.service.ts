import { Injectable, Injector } from "@angular/core";
import { Observable } from "rxjs";

import { GenericService } from "src/app/shared/services/generic.service";
import { environment } from "src/environments/environment";
import { GrupoCiclo } from "../model/grupo-ciclo.model";

@Injectable({
  providedIn: 'root'
})
export class GrupoService extends GenericService<GrupoCiclo> {

  constructor(protected injector: Injector) {
    super(injector, environment.apiCicloUrl)
    this.apiPath = 'veiculo';
  }
}
