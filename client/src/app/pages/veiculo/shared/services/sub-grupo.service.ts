import { Injectable, Injector } from "@angular/core";
import { Observable } from "rxjs";

import { GenericService } from "src/app/shared/services/generic.service";
import { environment } from "src/environments/environment";
import { SubGrupoCiclo } from "../model/sub-grupo-ciclo.model";

@Injectable({
  providedIn: 'root'
})
export class SubGrupoService extends GenericService<SubGrupoCiclo> {

  constructor(protected injector: Injector) {
    super(injector, environment.apiCicloUrl)
    this.apiPath = 'grupo';
  }

  serachVeiculo(codigoVeiculo: number, codigoGrupo: number): Observable<SubGrupoCiclo> {
    let options = { headers: this.getHeaders(), withCredentials: true };
    let url = `${this.getFullUrl()}/${codigoVeiculo}/${codigoGrupo}`;
    return this.http.get<SubGrupoCiclo>(url, options);
  }

}
