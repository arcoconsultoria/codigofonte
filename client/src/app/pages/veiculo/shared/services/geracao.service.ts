import { Injectable, Injector } from "@angular/core";
import { Observable } from "rxjs";
import { GenericService } from "src/app/shared/services/generic.service";

import { environment } from "../../../../../environments/environment";
import { GeracaoCiclo } from "../model/geracao-ciclo.model";
import { GeracoesCiclo } from "../model/geracoes-ciclo.model";

@Injectable({
  providedIn: 'root'
})
export class GeracaoService extends GenericService<GeracaoCiclo> {

  constructor(protected injector: Injector) {
    super(injector, environment.apiCicloUrl)
    this.apiPath = 'marca'
  }

  getAll(marca: string): Observable<GeracoesCiclo> {
    let options = { headers: this.getHeaders(), withCredentials: true };
    let url = this.getFullUrl() + '/' + marca
    return this.http.get<GeracoesCiclo>(url, options);
  }

}
