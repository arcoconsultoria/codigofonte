import { Versao } from "./versao.model";


export class Versoes extends Response {
    codigoModelo: number;
    descricaoModelo: string;
    tipoModelo: string;
    versoes: Array<Versao>

    constructor() {
        super()
        this.versoes = new Array<Versao>()
    }
}
