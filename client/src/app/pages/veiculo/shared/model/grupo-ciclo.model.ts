import { EntityCiclo } from "./entity-ciclo.model";
import { GrupoPecaCiclo } from "./grupo-peca-ciclo.model";

export class GrupoCiclo extends EntityCiclo {
  codigoVeiculo: number;
  anoModelo: number;
  grupoPecas: Array<GrupoPecaCiclo>;

  constructor() {
    super();
    this.grupoPecas = new Array<GrupoPecaCiclo>();
  }

}
