import { EntityCiclo } from "./entity-ciclo.model";
import { SubGrupoPecaCiclo } from "./sub-grupo-peca-ciclo.model";

export class SubGrupoCiclo extends EntityCiclo {
  codigoGrupo: number;
  descricaoGrupo: string;
  link: string;
  subgrupos: Array<SubGrupoPecaCiclo>;

  constructor() {
    super();
    this.subgrupos = new Array<SubGrupoPecaCiclo>();
  }

}
