import { EntityCiclo } from "./entity-ciclo.model";

export class GeracaoCiclo extends EntityCiclo {
  codigoGeracao: number;
  descricaoGeracao: string;
}
