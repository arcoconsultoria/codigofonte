import { Modelo } from "./modelo.model";

export class Modelos extends Response {
    codigoGeracao: number;
    descricaoGeracao: string;
    modelos: Array<Modelo>

    constructor() {
        super();
        this.modelos = new Array<Modelo>()
    }
}