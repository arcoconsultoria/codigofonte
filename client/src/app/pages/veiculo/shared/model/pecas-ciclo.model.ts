import { EntityCiclo } from "./entity-ciclo.model";
import { PecaCiclo } from "./peca-ciclo.model";

export class PecasCiclo extends EntityCiclo {
  codigoVeiculo: number;
  anoModelo: number;
  codigoGrupo: number;
  descricaoGrupo: string;
  codigoSubGrupo: number;
  descricaoSubGrupo: string;
  link: string;

  pecaCiclos: Array<PecaCiclo>;

  constructor() {
    super();
    this.pecaCiclos = new Array<PecaCiclo>();
  }

}
