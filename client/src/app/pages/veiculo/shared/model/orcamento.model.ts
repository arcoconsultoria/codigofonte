import { DataTransferObject } from "src/app/shared/models/data-transfer-object.model";
import { Base12 } from "./base12.model";

export class Orcamento extends DataTransferObject {
  codigoVeiculo: number;
  codigoMarca: number;
  marca: string;
  veiculo: Base12;
}
