import { EntityCiclo } from "./entity-ciclo.model";


export class Modelo extends EntityCiclo {
  codigoModelo: number;
  descricaoModelo: string;
  categoria: string;
  tipoModelo: string;
}
