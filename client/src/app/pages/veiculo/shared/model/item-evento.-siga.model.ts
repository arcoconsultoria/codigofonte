export class ItemEvento {
    anexo: Array<string>;
    numeroevento: string;
    placa: string;
    nomeassociado: string;
    logo: string;
    nomeassociacao: string;
    origem: string;
    sinid: number;
}