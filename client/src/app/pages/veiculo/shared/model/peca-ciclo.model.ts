import { EntityCiclo } from "./entity-ciclo.model";
import { PartNumberCiclo } from "./part-number-ciclo.model";

export class PecaCiclo extends EntityCiclo {
  codigoPecaCiclo: number;
  descricaoPecaCiclo: string;
  cdPecaCiclo: string;
  tmoPeca: number;
  tmoPintura: number;
  tmoReparacao: number;
  link: string;
  partNumber: Array<PartNumberCiclo>;

  constructor() {
    super();
    this.partNumber = new Array<PartNumberCiclo>();
  }
}
