import { EntityCiclo } from "./entity-ciclo.model";

export class GrupoPecaCiclo extends EntityCiclo {
  codigoGrupo: number;
  descricaoGrupo: string;
  link: string;
}
