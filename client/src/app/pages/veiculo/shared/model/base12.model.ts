export class Base12 {
  bdPecas: string;
  anoveiculoCiclo: number;
  codigoGeracao: number;
  codigoMarca: number;
  codigoModelo: number;
  codigoVeiculo: number;
  codigoVersao: number;
  fipe: string;
  geracao: string;
  link: string;
  linkDiant: string;
  linkTras: string;
  marca: string;
  modeloCiclo: string;
  procedencia: string;
  valorFipe: string;
  versionCiclo: string;
}
