import { EntityCiclo } from "./entity-ciclo.model";

export class VeiculoCodigo extends EntityCiclo {
    codigoVeiculo: number;
    anoModelo: number;
    bdPecas:string;
    fipe:string;
    valorFipe:number;
}