import { Entity } from "./entity.model";
import { ItemEvento } from "./item-evento.-siga.model";

export class Veiculo {
  codigo: number;
  placa: string;
  marca: Entity;
  geracao: Entity;
  modelo: Entity;
  versao: Entity;
  veiculo: Entity
  anoFabricacao: number;
  anoModelo: number;
  link: string;
  linkDiant: string;
  linkTras: string;

  getDescricao(): string {
    return `${this.modelo.descricao} ${this.versao.descricao} ${this.veiculo.linkImage}`
  }
  getTabelaFipe():any{
    return `${this.veiculo.linkImage}`
  }

}
