import { EntityCiclo } from "./entity-ciclo.model";

export class MarcaCiclo extends EntityCiclo {
  codigoMarca: number;
  descricaoMarca: string;
  link: string;
}
