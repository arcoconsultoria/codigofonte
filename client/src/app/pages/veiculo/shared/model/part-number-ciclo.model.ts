export class PartNumberCiclo {
  codigoPN: number;
  partNumber: string
  descricaoPN: string
  complemento: string
  qtd: number
  precoPeca: number
}
