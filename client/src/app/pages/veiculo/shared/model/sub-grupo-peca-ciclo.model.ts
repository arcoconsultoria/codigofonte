import { EntityCiclo } from "./entity-ciclo.model";

export class SubGrupoPecaCiclo extends EntityCiclo {
  codigoSubGrupo: number;
  descricaoSubGrupo: string;
  link: string;
}
