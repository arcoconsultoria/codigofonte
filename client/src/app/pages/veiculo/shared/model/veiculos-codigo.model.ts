import { VeiculoCodigo } from "./veiculo-codigo.model";

export class VeiculosCodigo extends Response {
    codigoVersao: number;
    descricaoVersao: string;
    linkDiant: string;
    linkTras: string;
    veiculos: Array<VeiculoCodigo>

    constructor() {
        super()
        this.veiculos = new Array<VeiculoCodigo>()
    }
}