import { GeracaoCiclo } from "./geracao-ciclo.model";

export class GeracoesCiclo extends Response {
  codigoMarca: number;
  descricaoMarca: string;
  link: string;
  geracoes: Array<GeracaoCiclo>;

  constructor() {
    super();
    this.geracoes = new Array<GeracaoCiclo>();
  }
}
