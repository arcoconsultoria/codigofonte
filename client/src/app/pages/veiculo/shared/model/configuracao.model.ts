export class Configuracao {
  precoPintura: number = 32.54;
  precoSubs: number = 40;
  precoReparo: number = 65.50;
  precoTroca: number = 10.50;
}