import { ItemEvento } from "./item-evento.-siga.model";
import { DataTransferObject } from "src/app/shared/models/data-transfer-object.model";

export class Evento extends DataTransferObject {
    success: boolean;
    msg: string;
    dados: ItemEvento;
}