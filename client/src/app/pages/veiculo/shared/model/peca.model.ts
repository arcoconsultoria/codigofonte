import { TipoServicoPeca } from "../enum/tipo-servico-peca.enum";

export class Peca {
    codigo: string;
    descricao: string;
    precoPeca: number
    tipoServicoExecutado: TipoServicoPeca;
    descricaoTipoServicoExecutado: string
    havePain: boolean
    tempoPeca: number
    tempoPintura: number
    tempoReparacao: number;
    valorTotalMaoObra: number
    valorPintura: number
    valorTotalBruto: number
    valorTotal: number

    constructor(){
        this.precoPeca = 0;
        this.havePain = false;
    }
}