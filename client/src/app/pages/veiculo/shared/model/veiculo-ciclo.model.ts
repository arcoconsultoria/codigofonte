import { Base12 } from './base12.model';
import { EntityCiclo } from "./entity-ciclo.model";

export class VeiculoCiclo extends EntityCiclo {
  anoFabricacao: number;
  anoModelo: number;
  chassi: string;
  combustivel: string;
  fabricante: string;
  modelo: string;
  placa: string;
  base12: Array<Base12>;

  constructor() {
    super();
    this.base12 = new Array<Base12>();
  }

}
