import { EntityCiclo } from "./entity-ciclo.model";

export class Versao extends EntityCiclo {
  codigoVersao: number;
  descricaoVersao: string;
  linkDiant: string;
  linkTras: string;
}
