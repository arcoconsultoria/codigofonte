import { Pipe, PipeTransform } from "@angular/core";
import { SubGrupoPecaCiclo } from "../model/sub-grupo-peca-ciclo.model";

@Pipe({ name: 'filterSubGrupo' })
export class FilterSubGrupoPipe implements PipeTransform {

  transform(items: Array<SubGrupoPecaCiclo>, searchText: string): Array<SubGrupoPecaCiclo> {
    if (!items) {
      return new Array<SubGrupoPecaCiclo>();
    }

    if (!searchText) {
      return items;
    }

    searchText = searchText.toLocaleLowerCase();

    return items.filter(i => {
      return i.descricaoSubGrupo.toLocaleLowerCase().includes(searchText)
    });
  }

}
