import { Pipe, PipeTransform } from "@angular/core";
import { PecaCiclo } from "../model/peca-ciclo.model";

@Pipe({ name: 'filterPeca' })
export class FilterPecaPipe implements PipeTransform {

  transform(items: Array<PecaCiclo>, searchText: string): Array<PecaCiclo> {
    if (!items) {
      return new Array<PecaCiclo>();
    }

    if (!searchText) {
      return items;
    }

    searchText = searchText.toLocaleLowerCase();

    return items.filter(i => {
      return i.descricaoPecaCiclo.toLocaleLowerCase().includes(searchText)
    });
  }

}
