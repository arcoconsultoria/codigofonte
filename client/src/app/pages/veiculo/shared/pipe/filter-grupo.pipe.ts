import { Pipe, PipeTransform } from "@angular/core";
import { GrupoPecaCiclo } from "../model/grupo-peca-ciclo.model";

@Pipe({ name: 'filterGrupo' })
export class FilterGrupoPipe implements PipeTransform {

  transform(items: Array<GrupoPecaCiclo>, searchText: string): Array<GrupoPecaCiclo> {
    if (!items) {
      return new Array<GrupoPecaCiclo>();
    }

    if (!searchText) {
      return items;
    }

    searchText = searchText.toLocaleLowerCase();

    return items.filter(i => {
      return i.descricaoGrupo.toLocaleLowerCase().includes(searchText)
    });
  }

}
