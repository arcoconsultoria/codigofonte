import { Injectable, Injector } from "@angular/core";
import { stringify } from "querystring";
import { Entity } from "./model/entity.model";
import { GeracaoCiclo } from "./model/geracao-ciclo.model";
import { GeracoesCiclo } from "./model/geracoes-ciclo.model";
import { GrupoCiclo } from "./model/grupo-ciclo.model";
import { GrupoPecaCiclo } from "./model/grupo-peca-ciclo.model";
import { MarcaCiclo } from "./model/marca-ciclo.model";
import { Modelo } from "./model/modelo.model";
import { Modelos } from "./model/modelos.models";


import { PecaCiclo } from "./model/peca-ciclo.model";
import { SubGrupoPecaCiclo } from "./model/sub-grupo-peca-ciclo.model";
import { VeiculosCodigo } from "./model/veiculos-codigo.model";
import { Versao } from "./model/versao.model";
import { Versoes } from "./model/versoes.model";
import { MarcaService } from "./services/marca.service";

@Injectable({
  providedIn: 'root'
})
export class WrapEntityService {

  constructor() {
  }

  wrapMarca(marcas: Array<MarcaCiclo>): Array<Entity> {
    let entityCliclos = new Array<Entity>();
    marcas.forEach(item => {
      let newEntity = new Entity();
      newEntity.codigo = item.codigoMarca;
      newEntity.descricao = item.descricaoMarca;
      newEntity.linkImage = item.link;
      entityCliclos.push(newEntity);
    })
    return entityCliclos;
  }

  wrapGeracao(geracao: GeracoesCiclo): Array<Entity> {
    let entityCliclos = new Array<Entity>();
    geracao.geracoes.forEach(item => {
      let newEntity = new Entity;
      newEntity.codigo = item.codigoGeracao;
      newEntity.descricao = item.descricaoGeracao;
      entityCliclos.push(newEntity)
    })

    return entityCliclos;
  }
  wrapModelo(modelo: Modelos): Array<Entity> {
    let entityCliclos = new Array<Entity>()
    modelo.modelos.forEach(item => {
      let newEntity = new Entity();
      newEntity.codigo = item.codigoModelo;
      newEntity.descricao = item.descricaoModelo;
      entityCliclos.push(newEntity);
    });
    return entityCliclos;
  }
  wrapVersao(versao: Versoes): Array<Entity> {
    let entityCliclos = new Array<Entity>()
    versao.versoes.forEach(item => {
      let newEntity = new Entity()
      newEntity.codigo = item.codigoVersao;
      newEntity.descricao = item.descricaoVersao;
      newEntity.linkImage = item.linkDiant
      newEntity.linkImage = item.linkTras
      entityCliclos.push(newEntity)
    })
    return entityCliclos
  }
  wrapVeiculo(veiculo: VeiculosCodigo): Array<Entity> {
    let entityCliclos = new Array<Entity>()
    veiculo.veiculos.forEach(item => {
      let newEntity = new Entity()
      newEntity.codigo = item.codigoVeiculo
      newEntity.descricao = item.anoModelo.toString()
      newEntity.linkImage = item.fipe
      entityCliclos.push(newEntity)
    })
    return entityCliclos
  }


  wrapGrupo(grupos: Array<GrupoPecaCiclo>): Array<Entity> {
    let entityCliclos = new Array<Entity>();
    grupos.forEach(item => {
      let newEntity = new Entity();
      newEntity.codigo = item.codigoGrupo;
      newEntity.descricao = item.descricaoGrupo;
      newEntity.linkImage = item.link;
      entityCliclos.push(newEntity);
    })
    return entityCliclos;
  }

  wrapSubGrupo(subGrupos: Array<SubGrupoPecaCiclo>): Array<Entity> {
    let entityCliclos = new Array<Entity>();
    subGrupos.forEach(item => {
      let newEntity = new Entity();
      newEntity.codigo = item.codigoSubGrupo;
      newEntity.descricao = item.descricaoSubGrupo;
      newEntity.linkImage = item.link;
      entityCliclos.push(newEntity);
    })
    return entityCliclos;
  }

  wrapPeca(peca: Array<PecaCiclo>): Array<Entity> {
    let entityCliclos = new Array<Entity>();
    peca.forEach(item => {
      let newEntity = new Entity();
      newEntity.codigo = item.codigoPecaCiclo
      newEntity.descricao = item.descricaoPecaCiclo
      newEntity.linkImage = item.link;
      entityCliclos.push(newEntity);
    })
    return entityCliclos;
  }

}
