import { Component, Input, OnInit } from '@angular/core';
import { AbstractControl, FormControl } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { TipoServicoPeca } from '../../shared/enum/tipo-servico-peca.enum';

@Component({
  selector: 'app-selecao-tipo-servico',
  templateUrl: './selecao-tipo-servico.component.html',
  styleUrls: ['./selecao-tipo-servico.component.css']
})
export class SelecaoTipoServicoComponent implements OnInit {

  @Input() tipoServicoExecutado: AbstractControl

  constructor(public bsModalRef: BsModalRef) { }

  ngOnInit(): void {
  }

  selecionouTipoServicoTroca(){
    this.selecionarTipoServico(TipoServicoPeca.Troca)
  }

  selecionouTipoServicoRemocao(){
    this.selecionarTipoServico(TipoServicoPeca.Subistituicao)
  }

  selecionouTipoServicoReparo(){
    this.selecionarTipoServico(TipoServicoPeca.Reparo)
  }

  private selecionarTipoServico(tipoServicoEscolhido: TipoServicoPeca): void{
    this.tipoServicoExecutado.patchValue(tipoServicoEscolhido, { emitEvent: true } )
    this.bsModalRef.hide()
  }

}
