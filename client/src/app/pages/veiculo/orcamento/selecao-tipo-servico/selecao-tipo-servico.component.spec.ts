import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SelecaoTipoServicoComponent } from './selecao-tipo-servico.component';

describe('SelecaoTipoServicoComponent', () => {
  let component: SelecaoTipoServicoComponent;
  let fixture: ComponentFixture<SelecaoTipoServicoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SelecaoTipoServicoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SelecaoTipoServicoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
