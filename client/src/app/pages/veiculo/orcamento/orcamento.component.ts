import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ButtonsModule } from 'ngx-bootstrap/buttons';

import { GrupoCiclo } from '../shared/model/grupo-ciclo.model';
import { GrupoPecaCiclo } from '../shared/model/grupo-peca-ciclo.model';
import { SubGrupoCiclo } from '../shared/model/sub-grupo-ciclo.model';
import { SubGrupoPecaCiclo } from '../shared/model/sub-grupo-peca-ciclo.model';
import { Veiculo } from '../shared/model/veiculo.model';
import { GrupoService } from '../shared/services/grupo.service';
import { SubGrupoService } from '../shared/services/sub-grupo.service';
import { SelectionStep } from '../shared/enum/selection-step.enum';
import { PecaService } from '../shared/services/peca.service';
import { PecasCiclo } from '../shared/model/pecas-ciclo.model';
import { PecaCiclo } from '../shared/model/peca-ciclo.model';
import { ListagemPecasComponent } from './listagem-pecas/listagem-pecas.component';
import { AlertService } from 'src/app/core/alert/shared/alert.service';
import { Configuracao } from '../shared/model/configuracao.model';
import { Utils } from 'src/app/shared/services/utils.service';
import { CurrencyPipe } from '@angular/common';
import { Peca } from '../shared/model/peca.model';
import { Evento } from '../shared/model/evento.model';
import { TipoServicoPeca } from '../shared/enum/tipo-servico-peca.enum';
import { SelecaoTipoServicoComponent } from './selecao-tipo-servico/selecao-tipo-servico.component';
import { NavBarComponent } from 'src/app/core/nav-bar/nav-bar.component';

@Component({
  selector: 'app-orcamento',
  templateUrl: './orcamento.component.html',
  styleUrls: ['./orcamento.component.css']
})
export class OrcamentoComponent implements OnInit {

  @Input() veiculo: Veiculo;
  @Input() configuracao: Configuracao;
  @Input() evento: Evento;

  resourceForm: FormGroup;
  resourceFormPeca: FormGroup;
  grupoCiclo: GrupoCiclo;
  subGrupoCiclo: SubGrupoCiclo;
  grupoPecaSelecionado: GrupoPecaCiclo;
  subGrupoPecaSelecioando: SubGrupoPecaCiclo;
  pecaCicloSelecionada: PecaCiclo;
  pecasCiclo: PecasCiclo;
  pathChoosePiece: Array<string>;
  step: Array<number>;
  pecas: Array<Peca>
  showSpinner: boolean;
  tempoReparoPecaSelecionada: number;
  selecinouTipoServico: boolean;

  bsModalRef: BsModalRef;

  constructor(
    private alertService: AlertService,
    private formBuilder: FormBuilder,
    private grupoService: GrupoService,
    private subGrupoService: SubGrupoService,
    private pecaService: PecaService,
    private modalService: BsModalService,
    private currencyPipe: CurrencyPipe
  ) { }

  ngOnInit(): void {
    this.buildResourceForm();
    this.loadGrupo();

    this.step = new Array<number>();
    this.step.push(SelectionStep.Grupo);

    this.pathChoosePiece = new Array<string>();
    this.pecas = new Array<Peca>();

    this.resourceForm = this.formBuilder.group({
      search: ''
    })

    this.selecinouTipoServico = false;
  }

  getStepCurrent(): number {
    return this.step[this.step.length - 1];
  }

  buildResourceForm() {
    this.resourceForm = this.formBuilder.group({
      elementos: this.formBuilder.array([
        this.formBuilder.group({
          codigo: [''],
          descricao: [''],
          link: [''],
        })
      ]),
    });

    this.resourceFormPeca = this.formBuilder.group({
      codigo: ['', Validators.required],
      descricao: ['', Validators.required],
      precoPeca: [0, Validators.required],
      tempoPeca: ['00:00', Validators.required],
      havePain: [false, Validators.required],
      tempoPintura: ['00:00'],
      tipoServicoExecutado: [null, Validators.required],
      descricaoTipoServicoExecutado: ['', Validators.required],
      valorMaoDeObraPintura: [0],
      valorMaoDeObraServico: [0, Validators.required],
      valorTotalMaoDeObra: [0, Validators.required],
      valorPintura: [0],
      valorTotalBruto: [0, Validators.required],
      valorTotal: [0, Validators.required],
    })

    this.resourceFormPeca.controls.havePain.valueChanges.subscribe( () => this.calcularValorMaoObraPintura() )
    this.resourceFormPeca.controls.tempoPintura.valueChanges.subscribe( () => this.calcularValorMaoObraPintura() )
    this.resourceFormPeca.controls.tempoPeca.valueChanges.subscribe( () => this.calcularValorMaoDeObraServico() )
    this.resourceFormPeca.controls.tipoServicoExecutado.valueChanges.subscribe( () => this.setDescricaoTipoServico() )

    // Tentativa do valor ser apresentado no formato brasileiro
    // this.resourceFormPeca.valueChanges.subscribe( form => {
    //   if (form.precoPeca) {
    //       console.log(form.precoPeca);
    //     this.resourceFormPeca.patchValue({          
    //       precoPeca: this.currencyPipe.transform( form.precoPeca.replace(/\D/g, '').replace(/^0+/, ''), 'BRL', 'symbol', '1.0-0')
    //     }, {emitEvent: false})
    //   }
    // } )

  }

  loadGrupo() {
    this.activateSpinner();
    this.grupoService.serach(String(this.veiculo.codigo)).subscribe(
      result => {
        //let entityCiclo = this.wrapService.wrapGrupo(result.grupoPecas);
        //this.resourceForm.get('elementos').patchValue(entityCiclo)  ///,
        if (result.grupoPecas.length == 0) {
          this.alertService.error('Não foi possivel localizar o grupo de peças.')
        }
        this.grupoCiclo = result
      },
      (error) => console.log(error),
      () => this.deactivateSpinner()
    );
  }

  getGrupoPecas(): Array<GrupoPecaCiclo> {
    return this.grupoCiclo?.grupoPecas;
  }

  selecionaGrupoPeca(grupoPeca: GrupoPecaCiclo) {
    this.grupoPecaSelecionado = grupoPeca;
    this.step.push(SelectionStep.SubGrupo)
    this.addPatch(grupoPeca.descricaoGrupo);
    this.loadSubGrupo();
    this.cleanSearch();
  }

  cleanSearch() {
    this.resourceForm.controls.search.setValue('');
  }

  addPatch(texto: string) {
    this.pathChoosePiece.push(texto);
  }

  removePatch() {
    this.pathChoosePiece.pop();
  }

  loadSubGrupo() {
    this.activateSpinner();
    this.subGrupoService.serachVeiculo(this.veiculo.codigo, this.grupoPecaSelecionado.codigoGrupo).subscribe(
      result => this.subGrupoCiclo = result,
      (error) => console.log(error),
      () => this.deactivateSpinner()
    )
  }

  selecionaSubGrupoPeca(subGrupoPeca: SubGrupoPecaCiclo) {
    this.subGrupoPecaSelecioando = subGrupoPeca;
    this.step.push(SelectionStep.Peca)
    this.addPatch(subGrupoPeca.descricaoSubGrupo);
    this.loadPecas();
    this.cleanSearch();
  }

  loadPecas() {
    this.activateSpinner();
    this.pecaService.serachVeiculo(this.veiculo.codigo, this.subGrupoPecaSelecioando.codigoSubGrupo)
      .subscribe(
        result => this.pecasCiclo = result,
        (error) => console.error(error),
        () => this.deactivateSpinner());
  }

  getSubGrupoPecas() {
    return this.subGrupoCiclo?.subgrupos;
  }

  getPecas(): Array<PecaCiclo> {
    return this.pecasCiclo?.pecaCiclos;
  }

  selecionaPeca(peca: PecaCiclo) {
    this.pecaCicloSelecionada = peca;
    this.step.push(SelectionStep.AddPeca)
    this.addPatch(peca.descricaoPecaCiclo);
    this.cleanSearch();
    
    this.openModalSelecaoTipoServico();    
    // Quando fechar o modal, preencher o formulário de peca
    this.bsModalRef.onHide.subscribe( () => this.setFormPecaSelecionada() )

  }

  setFormPecaSelecionada(){
    this.resourceFormPeca.controls.codigo.setValue(this.getFirstPartNumber());
    this.resourceFormPeca.controls.descricao.setValue(this.pecaCicloSelecionada.descricaoPecaCiclo);
    this.resourceFormPeca.controls.precoPeca.setValue(this.getPrecoPeca());
    this.setTempos()
  }

  backStep() {
    this.resourceFormPeca.reset();
    this.step.pop();
    this.removePatch();
  }

  nextStep() {
    this.step.push(SelectionStep.Confirmacao);
  }

  hasStep(): boolean {
    return this.step.length > 1
  }

  getPrecoPeca(): number {
    let preco: number = 0;
    if (this.pecaCicloSelecionada?.partNumber?.length > 0) {
      preco = this.pecaCicloSelecionada.partNumber[0].precoPeca;
    }
    return preco
  }

  private setTempos() {
    
    let tempoCentesimal : number = 0;

    if (this.hasRepair()) {
      tempoCentesimal = this.pecaCicloSelecionada.tmoReparacao
    } else if (this.hasSwap() ){
      tempoCentesimal = this.pecaCicloSelecionada.tmoPeca
    }
    
    this.resourceFormPeca.controls.tempoPeca.setValue(this.calcularHoraSexagesimal(tempoCentesimal))

    tempoCentesimal = this.pecaCicloSelecionada.tmoPintura
    if (tempoCentesimal > 0){
      this.resourceFormPeca.controls.tempoPintura.setValue(this.calcularHoraSexagesimal(tempoCentesimal))
    }
  }

  calcularHoraCentesimal(horaStr: string): number {
    let horaCentesimal = 0
    if (horaStr) {
      let hora: number = Number(horaStr.split(':', 2)[0]);
      let minHot: number = Number(horaStr.split(':', 2)[1]) / 60.00;
      horaCentesimal = hora + minHot;
    }
    return horaCentesimal;
  }

  calcularHoraSexagesimal(tempo: number): string {
    let horaSexagesimal = ""
    if (tempo > 0){
      let tempoStr = tempo.toString().split('.');
      let hora = tempoStr[0].padStart(2, '0');

      let minHot: string = '00'
      if (tempoStr.length > 1){
        minHot = (  Math.round(((Number(tempoStr[1]) * 60 ) / 100), ) ).toString().padStart(2, '0');
      }
      
      horaSexagesimal = `${hora}:${minHot}`;
    }
    return horaSexagesimal
  }

  calcularValorMaoDeObraServico(){
    let valorMaoDeObraServico: number = 0;
    let horaCentesimal = this.calcularHoraCentesimal(this.getTempoPeca())

    if (this.hasSwap()){
      valorMaoDeObraServico = Utils.roundCurrency(horaCentesimal * this.configuracao.precoTroca)
    }

    if (this.hasRepair()) {
      valorMaoDeObraServico = Utils.roundCurrency(horaCentesimal * this.configuracao.precoReparo);
    }

    if (this.hasReplacement()) {
      valorMaoDeObraServico = Utils.roundCurrency(horaCentesimal * this.configuracao.precoSubs)
    }

    this.resourceFormPeca.controls.valorMaoDeObraServico.setValue(valorMaoDeObraServico);
    this.calcularTotal();
  }
  
  getTempoPeca(): string {
    return this.resourceFormPeca.controls.tempoPeca.value;
  }

  getTempoPintura(): string {
    return this.resourceFormPeca.controls.tempoPintura.value;
  }

  calcularValorMaoObraPintura() {
  let valorMaoObra: number = 0;
  let tempoPintura: number = 0;

    if (this.hasPain()){
      tempoPintura = this.calcularHoraCentesimal(this.resourceFormPeca.controls.tempoPintura.value)
      valorMaoObra = Utils.roundCurrency(tempoPintura * this.configuracao.precoPintura);
    } 

    this.resourceFormPeca.patchValue({
      valorMaoDeObraPintura: valorMaoObra
    }, { emitEvent: false })
    
    this.calcularTotal();
  }

  calcularTotal() {
    let precoPeca = this.resourceFormPeca.controls.precoPeca.value as number
    let valorlMaoDeObraServico = this.resourceFormPeca.controls.valorMaoDeObraServico.value as number;
    let valorMaoDeObraPintura = this.resourceFormPeca.controls.valorMaoDeObraPintura.value as number
    let valorTotal: number = 0

    if ( this.hasSwap() ){
      valorTotal = precoPeca + valorlMaoDeObraServico + valorMaoDeObraPintura;
    } else {
      valorTotal = valorlMaoDeObraServico + valorMaoDeObraPintura
    }

    this.resourceFormPeca.patchValue({
      valorTotalBruto: Utils.roundCurrency(valorTotal),
      valorTotal: Utils.roundCurrency(valorTotal) ,
      valorTotalMaoDeObra: Utils.roundCurrency(valorMaoDeObraPintura + valorlMaoDeObraServico)
    })

  }

  adicionarPeca() {
    let peca: Peca = this.resourceFormPeca.value as Peca;
    this.pecas.push(peca)
    this.backStep();
  }

  clearPatch() {
    this.pathChoosePiece = new Array<string>();
  }

  getQtdPecasAdd(): number {
    return this.pecas.length
  }

  hasAddPeca(): boolean {
    return this.pecas.length > 0;
  }

  openModalPecas() {
    const initialState = {
      pecas: this.pecas,
      veiculo: this.veiculo // o problema era esse, vc não tinha passado o veiculo para o modal
    };
    this.bsModalRef = this.modalService.show(ListagemPecasComponent, { initialState, class: 'modal-dialog modal-xl' });
  }

  openModalSelecaoTipoServico(){
    const initialState = {
      tipoServicoExecutado: this.resourceFormPeca.controls.tipoServicoExecutado
      //podeSerReparada: this.pecaCicloSelecionada.tmoReparacao > 0
    };
    this.bsModalRef = this.modalService.show(SelecaoTipoServicoComponent, { initialState, class: 'modal-dialog-centered modal-lg' });
  }

  setDescricaoTipoServico() {
    let tipoServicoExecutado = this.resourceFormPeca.controls.tipoServicoExecutado.value
    let descricao = "";
    switch(tipoServicoExecutado){
      case TipoServicoPeca.Troca:
        descricao = "Troca"
        break
      case TipoServicoPeca.Subistituicao:
        descricao = "Subistituição"
        break
      case TipoServicoPeca.Reparo:
        descricao = "Reparo"
        break
      default:
        descricao = "Tipo Não Encontrado" 
        break
    }

    this.resourceFormPeca.controls.descricaoTipoServicoExecutado.setValue(descricao);
  }

  activateSpinner() {
    this.showSpinner = true;
  }

  deactivateSpinner() {
    this.showSpinner = false;
  }

  // TODO, este mesmo metodo e utilizado tbm no componente ListagemPecas, deveria ser um unico codigo
  getFirstPartNumber(): string {
    return this.pecaCicloSelecionada != null && this.pecaCicloSelecionada.partNumber.length > 0
      ? this.pecaCicloSelecionada.partNumber[0].partNumber
      : '-'
  }

  getTextSearch(): string {
    return this.resourceForm.controls.search.value;
  }

  hasRepair(): boolean {
    return this.resourceFormPeca.controls.tipoServicoExecutado.value == TipoServicoPeca.Reparo
  }

  hasReplacement(): boolean {
    return this.resourceFormPeca.controls.tipoServicoExecutado.value == TipoServicoPeca.Subistituicao
  }

  hasSwap(): boolean {
    return this.resourceFormPeca.controls.tipoServicoExecutado.value == TipoServicoPeca.Troca
  }

  hasPain(): boolean {
    return this.resourceFormPeca?.controls?.havePain?.value;
  }

  hasErrorFormPeca():boolean {
    return Utils.containsError(this.resourceFormPeca);
  }

}
