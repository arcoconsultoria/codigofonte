import { Component, Input, OnInit } from '@angular/core';
import { CircleProgressComponent } from 'ng-circle-progress';
import { BsModalRef,ModalModule,BsModalService } from 'ngx-bootstrap/modal';
import { Observable } from 'rxjs';
import { PecaCiclo } from '../../shared/model/peca-ciclo.model';
import { Peca } from '../../shared/model/peca.model';
import { VeiculoCodigo } from '../../shared/model/veiculo-codigo.model';
import { Veiculo } from '../../shared/model/veiculo.model';
import { VeiculoCodigoService } from '../../shared/services/veiculo-codigo.service';



@Component({
  selector: 'app-listagem-pecas',
  templateUrl: './listagem-pecas.component.html',
  styleUrls: ['./listagem-pecas.component.css']
})
export class ListagemPecasComponent implements OnInit {

  
  veiculo:Veiculo
  pecas: Array<Peca>;
  pecaAhEditar: Peca;

  

  constructor(
    public bsModalRef: BsModalRef,    
    ) {
    
  }

  ngOnInit(): void { 
    this.getTabela()
  }

  getTabela():string{
   return this.veiculo?.getTabelaFipe()
    
  }
  getValorTotal(): number {
    let total = this.pecas.reduce((sum, current) => sum + current?.valorTotal, 0)
    return total;
  }
  
  colorsTbFipe(){
    let success0:string= "bg-success"
    let warning50:string="bg-warning text-dark"
    let danger75:string="bg-danger"
    let percentage =this.getPorcentagem()

    if(percentage <= 49){
      return success0
    }else if(percentage <=74){
      return warning50
    }else if(percentage >= 75){
      return danger75
    }
    
  }
  getPorcentagem(){
    let tF = this.veiculo.getTabelaFipe()
    let total1 = this.pecas.reduce((sum, current) => sum + current?.valorTotal, 0)
    let x= 100
    let cal= ((total1*x)/(tF)) 
    return  cal
  }
  
  removerPeca(peca: Peca){
    let index = this.pecas.indexOf(peca, 0)
    if (index > -1){
      this.pecas.splice(index, 1)
    }
  }

  editarPeca(peca: Peca){
    this.pecaAhEditar = peca;
    this.bsModalRef.hide();
  }
  
  
}
