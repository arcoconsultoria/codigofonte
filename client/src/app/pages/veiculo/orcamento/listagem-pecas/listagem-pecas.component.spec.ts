import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListagemPecasComponent } from './listagem-pecas.component';

describe('ListagemPecasComponent', () => {
  let component: ListagemPecasComponent;
  let fixture: ComponentFixture<ListagemPecasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListagemPecasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListagemPecasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
