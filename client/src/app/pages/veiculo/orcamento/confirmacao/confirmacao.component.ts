import { Component, Input, OnInit } from '@angular/core';
import { Peca } from '../../shared/model/peca.model';

@Component({
  selector: 'app-confirmacao',
  templateUrl: './confirmacao.component.html',
  styleUrls: ['./confirmacao.component.css']
})
export class ConfirmacaoComponent implements OnInit {

  @Input() pecasSelecionadas: Array<Peca>

  constructor() { }

  ngOnInit(): void {
  }

  getTotalOrcamento(): number {
    let total = this.pecasSelecionadas.reduce((sum, current) => sum + current?.valorTotal, 0)
    return total;
  }
}
