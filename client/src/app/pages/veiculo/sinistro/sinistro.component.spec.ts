import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SinistroComponent } from './sinistro.component';

describe('SinistroComponent', () => {
  let component: SinistroComponent;
  let fixture: ComponentFixture<SinistroComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SinistroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SinistroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
