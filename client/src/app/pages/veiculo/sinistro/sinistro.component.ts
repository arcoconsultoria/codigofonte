import { Component, Input, OnInit, TemplateRef } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Evento } from '../shared/model/evento.model';
import { Veiculo } from '../shared/model/veiculo.model';

@Component({
  selector: 'app-sinistro',
  templateUrl: './sinistro.component.html',
  styleUrls: ['./sinistro.component.css']
})
export class SinistroComponent implements OnInit {

  @Input() veiculo: Veiculo;
  @Input() evento: Evento;

  anexos: Array<string> = new Array<string>();
  modalRef?: BsModalRef;

  constructor(
    private modalService: BsModalService
  ) { }

  ngOnInit(): void {
  }

  getImageMarca(): string{
    return this.veiculo?.marca?.linkImage;
  }

  getLogoAssociacao(): string {
    return  this.getImageBase64(this.evento.dados.logo)
  }

  getImageBase64(base64: string): string {
    return `data:image/jpeg;base64,${base64}`
  }

  opemModalAnexos(modalAnexo: TemplateRef<any>): void {
    // const initialState = {
    //   anexos: this.evento.dados.anexo
    // };
    this.modalRef = this.modalService.show(modalAnexo, { class: 'modal-dialog modal-xl' } );
  }

}
