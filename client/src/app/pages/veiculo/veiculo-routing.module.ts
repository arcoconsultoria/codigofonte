import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OrcamentoComponent } from './orcamento/orcamento.component';
import { VeiculoFormComponent } from './veiculo-form/veiculo-form.component';

const routes: Routes = [
  {
    path: '', component: VeiculoFormComponent
  },
  {
    path: 'orcamento', component: OrcamentoComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VeiculoRoutingModule { }
