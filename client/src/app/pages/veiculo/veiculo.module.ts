import { NgModule } from '@angular/core';
import { ModalModule } from 'ngx-bootstrap/modal';

import { SharedModule } from '../../shared/shared.module'
import { VeiculoRoutingModule } from './veiculo-routing.module';
import { VeiculoFormComponent } from './veiculo-form/veiculo-form.component';
import { OrcamentoComponent } from './orcamento/orcamento.component';
import { SinistroComponent } from './sinistro/sinistro.component';
import { EditVeiculoFormComponent } from './veiculo-form/edit-veiculo-form/edit-veiculo-form.component';
import { ListagemPecasComponent } from './orcamento/listagem-pecas/listagem-pecas.component';
import { SelectBase12Component } from './veiculo-form/select-base12/select-base12.component';
import { FilterGrupoPipe } from './shared/pipe/filter-grupo.pipe';
import { FilterSubGrupoPipe } from './shared/pipe/filter-sub-grupo.pipe';
import { FilterPecaPipe } from './shared/pipe/filter-peca.pipe';
import { ConfirmacaoComponent } from './orcamento/confirmacao/confirmacao.component';
import { SelecaoTipoServicoComponent } from './orcamento/selecao-tipo-servico/selecao-tipo-servico.component';
import { NgCircleProgressModule } from 'ng-circle-progress';

@NgModule({
  declarations: [VeiculoFormComponent,
    OrcamentoComponent,
    SinistroComponent,
    EditVeiculoFormComponent,
    ListagemPecasComponent,
    SelectBase12Component,
    FilterGrupoPipe,
    FilterSubGrupoPipe,
    FilterPecaPipe,
    ConfirmacaoComponent,
    SelecaoTipoServicoComponent],
  imports: [
    SharedModule,
    VeiculoRoutingModule,
    ModalModule.forRoot(),
    NgCircleProgressModule.forRoot({
      // set defaults here
      radius: 45,
      outerStrokeWidth: 4,
      innerStrokeWidth: 8,
      outerStrokeColor: "#0B5ED7",
      innerStrokeColor: "#000000",
      animationDuration: 300,
      showBackground: false,
      startFromZero: false,
      showTitle:true,
      showSubtitle:false,
      
    })
  ]
})
export class VeiculoModule { }
