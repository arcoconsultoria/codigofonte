import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, RequiredValidator, Validators } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { Subject } from 'rxjs';
import { Base12 } from '../../shared/model/base12.model';

@Component({
  selector: 'app-select-base12',
  templateUrl: './select-base12.component.html',
  styleUrls: ['./select-base12.component.css']
})
export class SelectBase12Component implements OnInit {

  base12Selected: Subject<Base12>
  bases: Array<Base12>;
  resourceForm: FormGroup;

  constructor(public bsModalRef: BsModalRef,
    private formBuild: FormBuilder) { }

  ngOnInit(): void {

    this.base12Selected = new Subject();

    this.resourceForm = this.formBuild.group({
      idBase12Selected: ['', Validators.required]
    })

  }

  submitForm() {
    this.base12Selected.next(this.bases[this.getIdBase12Select()]);
    this.bsModalRef.hide()
  }

  getIdBase12Select(): number {
    return this.resourceForm.controls.idBase12Selected.value as number;
  }

}
