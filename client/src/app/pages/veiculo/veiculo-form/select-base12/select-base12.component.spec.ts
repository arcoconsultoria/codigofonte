import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectBase12Component } from './select-base12.component';

describe('SelectBase12Component', () => {
  let component: SelectBase12Component;
  let fixture: ComponentFixture<SelectBase12Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SelectBase12Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectBase12Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
