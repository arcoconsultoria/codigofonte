import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

import { ModalFormComponent } from 'src/app/shared/components/modal-form/modal-form.component';
import { Base12 } from '../shared/model/base12.model';
import { Configuracao } from '../shared/model/configuracao.model';
import { Entity } from '../shared/model/entity.model';
import { Evento } from '../shared/model/evento.model';
import { VeiculoCiclo } from '../shared/model/veiculo-ciclo.model';
import { Veiculo } from '../shared/model/veiculo.model';
import { GeracaoService } from '../shared/services/geracao.service';
import { MarcaService } from '../shared/services/marca.service';
import { ModeloService } from '../shared/services/modelo.service';
import { SigaService } from '../shared/services/siga.service';
import { VeiculoCodigoService } from '../shared/services/veiculo-codigo.service';
import { VeiculoService } from '../shared/services/veivulo.service'
import { VersaoService } from '../shared/services/versao.service';
import { WrapEntityService } from '../shared/wrap-entity-ciclo.service';
import { SelectBase12Component } from './select-base12/select-base12.component';

@Component({
  selector: 'app-veiculo-form',
  templateUrl: './veiculo-form.component.html',
  styleUrls: ['./veiculo-form.component.css']
})
export class VeiculoFormComponent implements OnInit {

  currentIdEvento: number;
  currentEvento: Evento;
  currentIdVeiculo: number;
  currentAction: string;
  resource: Veiculo;
  configuracao: Configuracao;
  veiculoConsulta: VeiculoCiclo;
  resourceForm: FormGroup;
  serverErrorMessages: string[] = null;
  showSpinner: boolean = false;
  showModalVeiculo: boolean = false;
  desableMarca: boolean;
  desableGeracao: boolean;
  desableModelo: boolean;
  desableVersao: boolean;
  desableVeiculo: boolean;
  bsModalRef: BsModalRef;
  validarVeiculo: boolean;

  marcas: Array<Entity>;
  geracoes: Array<Entity>;
  modelos: Array<Entity>;
  versoes: Array<Entity>;
  veiculos: Array<Entity>;

  constructor(private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private resourceService: VeiculoService,
    private sigaService: SigaService,
    private marcaService: MarcaService,
    private geracaoService: GeracaoService,
    private modeloService: ModeloService,
    private versaoService: VersaoService,
    private veiculoService: VeiculoCodigoService,
    private wrapEntityCiclo: WrapEntityService,
    private modalService: BsModalService) {
  }

  ngOnInit(): void {
    this.buildResourceForm();
    this.loadEvento();
    this.validarVeiculo = true;
    this.desableMarca = true;
    this.desableGeracao = true;
    this.desableModelo = true;
    this.desableVersao = true;
    this.desableVeiculo= true
    this.configuracao = new Configuracao();
  }

  buildResourceForm() {
    this.resourceForm = this.formBuilder.group({
      placa: ['', Validators.required],
      marca: this.formBuilder.group({
        codigo: ['', Validators.required],
        descricao: ['', Validators.required],
        linkImage: [''],
      }),
      geracao: this.formBuilder.group({
        codigo: ['', Validators.required],
        descricao: ['', Validators.required],
        linkImage: [''],
      }),
      modelo: this.formBuilder.group({
        codigo: ['', Validators.required],
        descricao: ['', Validators.required],
        linkImage: [''],
      }),
      versao: this.formBuilder.group({
        codigo: ['', Validators.required],
        descricao: ['', Validators.required],
        linkImage: [''],
      }),
      veiculo: this.formBuilder.group({
        codigo: ['',Validators.required],
        descricao: ['',Validators.required],
        linkImage: [''],
      })
    });
  }

  loadEvento() {
    this.currentIdEvento = Number(this.route.snapshot.queryParamMap.get('sinistro'));
    this.sigaService.getEvento(this.currentIdEvento).subscribe(result => {
      this.currentEvento = result
      this.loadResource();
    })
  }

  loadResource() {
    this.activateSpinner();
    this.resourceService.consultaPlaca(this.currentEvento?.dados.placa).subscribe(
      (result) => {
        if (result?.message) {
          this.resource = new Veiculo();
          this.resource.placa = this.currentEvento.dados.placa;
        } else {
          this.veiculoConsulta = result;
          this.resource = this.getVeiculo();
          this.crivoSelecaoBase12();
          this.resourceForm.patchValue(this.resource);
        }
        this.validateDadosVeiculo();
        this.deactivateSpinner()
      },
      (error: HttpErrorResponse) => this.actionsFromError(error),
      () => this.deactivateSpinner()
    )
  }

  getVeiculo(): Veiculo {
    let veiculo: Veiculo = new Veiculo();
    veiculo.placa = this.veiculoConsulta.placa;
    veiculo.anoModelo = this.veiculoConsulta.anoModelo;
    veiculo.anoFabricacao = this.veiculoConsulta.anoFabricacao;
    return veiculo;
  }

  crivoSelecaoBase12() {
    if (this.veiculoConsulta?.base12) {
      if (this.veiculoConsulta.base12.length > 1) {
        this.showModalSelectBase12();
      } else if (this.veiculoConsulta.base12.length == 1) {
        let base12Selected = this.veiculoConsulta.base12[0];
        this.setBase12Selected(base12Selected);
      }
    }
  }

  setBase12Selected(base12Selected: Base12) {
    this.resource.codigo = base12Selected.codigoVeiculo;
    this.resource.marca = this.getEnityCiclo(base12Selected.codigoMarca, base12Selected.marca, base12Selected.link);
    this.resource.geracao = this.getEnityCiclo(base12Selected.codigoGeracao, base12Selected.geracao);
    this.resource.modelo = this.getEnityCiclo(base12Selected.codigoModelo, base12Selected.modeloCiclo);
    this.resource.versao = this.getEnityCiclo(base12Selected.codigoVersao, base12Selected.versionCiclo);
    this.resource.veiculo= this.getEnityCiclo(base12Selected.codigoVeiculo, String(base12Selected.anoveiculoCiclo), base12Selected.valorFipe) 
    this.resourceForm.patchValue(this.resource)
  }

  showModalSelectBase12() {
    let initialState = {
      bases: this.veiculoConsulta.base12
    };
    this.bsModalRef = this.modalService.show(SelectBase12Component, { initialState, class: 'modal-dialog', backdrop: "static" });
    this.bsModalRef.content.base12Selected.subscribe(result => this.setBase12Selected(result));
  }

  getEnityCiclo(codigo: number, descricao: string, linkImage?: string): Entity {
    let entity = new Entity();
    entity.codigo = codigo;
    entity.descricao = descricao;
    entity.linkImage = linkImage;

    return entity;
  }

  validateDadosVeiculo() {
    if (!this.existsVeiculoCiclo() || !this.existsBase12()) {
      this.showModalVeiculo = true;
      //this.desableRead = false;
      this.openModalWithComponent();
      this.loadMarcas();
    }
  }

  loadMarcas() {
    this.marcaService.loadAll().subscribe(result => this.marcas = this.wrapEntityCiclo.wrapMarca(result));
  }
  loadGeracao() {
    this.geracaoService.getAll(this.resourceForm.controls.marca.value.codigo).subscribe(result => this.geracoes = this.wrapEntityCiclo.wrapGeracao(result));
  }
  loadModelo() {
    this.modeloService.getAll(this.resourceForm.controls.geracao.value.codigo).subscribe(result => this.modelos = this.wrapEntityCiclo.wrapModelo(result));
  }
  loadVersao() {
    this.versaoService.getAll(this.resourceForm.controls.modelo.value.codigo).subscribe(result => this.versoes = this.wrapEntityCiclo.wrapVersao(result))
  }
  loadVeiculo() {
    this.veiculoService.getAll(this.resourceForm.controls.versao.value.codigo).subscribe(result => this.veiculos = this.wrapEntityCiclo.wrapVeiculo(result))
  }

  private existsVeiculoCiclo(): boolean {
    return this.resource.placa != null && this.resource.placa != '';
  }

  private existsBase12(): boolean {
    return this.veiculoConsulta?.base12?.length >= 1;
  }

  actionsFromError(error: HttpErrorResponse): void {
    console.log(error.message);
  }

  activateSpinner() {
    this.showSpinner = true;
  }

  deactivateSpinner() {
    this.showSpinner = false;
  }

  submitForm() {
    this.validarVeiculo = false;
  }

  alterarDados() {
    this.desableMarca = false;
    this.desableGeracao = false;
    this.desableModelo = false;
    this.desableVersao = false;
    this.loadMarcas();
    this.loadGeracao();
    this.loadModelo();
    this.loadVersao();
    this.loadVeiculo();


    //this.resourceForm.patchValue(this.resource);
  }

  openModalWithComponent() {
    let initialState = {
      title: 'Veiculo não encontrado !',
      description: 'Não foi possível validar os dados do veiculo, favor informar os dados manualmente.',
    };
    this.bsModalRef = this.modalService.show(ModalFormComponent, { initialState });
    this.bsModalRef.content.closeBtnName = 'Fechar';
  }
}
