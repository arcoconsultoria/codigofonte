import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditVeiculoFormComponent } from './edit-veiculo-form.component';

describe('EditVeiculoFormComponent', () => {
  let component: EditVeiculoFormComponent;
  let fixture: ComponentFixture<EditVeiculoFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditVeiculoFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditVeiculoFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
