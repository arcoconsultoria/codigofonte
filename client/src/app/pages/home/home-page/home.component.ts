import { Component, OnInit } from '@angular/core';
import { SessionService } from './../../../shared/services/session.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {



  constructor(private session: SessionService,
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit(): void {

  }

  logoff(){
    this.session.clear();
    this.router.navigate(['/login/login-form']);
  }

}

