import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators  } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageService } from 'src/app/shared/message.service';
import { RestService } from 'src/app/shared/services/rest.service';
import { SessionService } from 'src/app/shared/services/session.service';
import { Entity } from '../veiculo/shared/model/entity.model';

@Component({
  selector: 'app-empresa',
  templateUrl: './empresa.component.html',
  styleUrls: ['./empresa.component.css']
})
export class EmpresaComponent implements OnInit {
  resourceForm:FormGroup;
  isReadOnly:boolean=false;
  states:Array<any> = [];
  citys:Array<any>=[];
  represent_citys:Array<any>=[];
  genders:Array<any> = [{value:'M',label:'Feminino'},{value:'M',label:'Masculino'}];

  //estado civil array
  marital_status:Array<any> = [ {value:'Solteiro',label:'Solteiro'},
                                {value:'Casado',label:'Casado'},
                                {value:'Separado',label:'Separado'},
                                {value:'Divorciado',label:'Divorciado'},
                                {value:'Viúvo',label:'Viúvo'}
                              ];
  constructor(private route: ActivatedRoute,
              private router: Router,
              //private loginService: LoginService,
              private sessionService: SessionService,
              private formBuilder: FormBuilder,
              private rest:RestService,
              private info:MessageService) {                
               
   }

  ngOnInit(): void {
    this.buildResourceForm()
  }

  onChangeUF(event){
    this.loadCitys();
  }

  onChangeRepresentUF(event){
    this.loadRepresentCitys();
  }


  buildResourceForm() {
    
     //validator form alterar senha
     this.resourceForm = this.formBuilder.group({
      name: [null, [Validators.required]],
      companyName: [null, [Validators.required]],
      cnpj: [null, [Validators.required]],
      inscEstadual: [null, []],
      establishmentDate: [null, [Validators.required]],
      //contato
      phoneNumber: [null, []],
      celPhoneNumber: [null, []],
      email: [null, [Validators.email]],
      
      //endereço
      address: [null, [Validators.required]],
      number: [null, [Validators.required]],
      complement: [null, [Validators.required]],
      cep: [null, [Validators.required]],
      neighborhood: [null, []],
      state: [null, [Validators.required]],
      idCity: [null, [Validators.required]],

        //represent ------------------------------------
        represent_name: [null, [Validators.required]],
        represent_taxId: [null, [Validators.required]],
        represent_birthDate: [null, [Validators.required]],
        represent_gender: [null, [Validators.required]],
        represent_marital_status: [null, [Validators.required]],
        represent_nationality_state: [null, [Validators.required]],
        represent_document_number: [null, [Validators.required]],
        represent_document_state: [null, [Validators.required]],
        represent_orgao_emissor: [null, [Validators.required]],
        represent_issuance_date: [null, [Validators.required]],
        represent_phoneNumber: [null, [Validators.required]],
        represent_email: [null, [Validators.email]],
        //endereço dor respresentante
        represent_address: [null, [Validators.required]],
        represent_number: [null, [Validators.required]],
        represent_complement: [null, [Validators.required]],
        represent_cep: [null, [Validators.required]],
        represent_neighborhood: [null, []],
        represent_state: [null, [Validators.required]],
        represent_idCity:[null, [Validators.required]],

      
      });

      this.alterarDados();
  }

  submitForm(){

    let postData ={
      "client":{
         "name": this.resourceForm.controls.name,
         "taxIdentifier":{
            "taxId":this.resourceForm.controls.taxId,
            "inscEstadual":this.resourceForm.controls.inscEstadual,
            "country":"BRA"
         },
         "numberPhone":{
            "phoneNumber":this.resourceForm.controls.phoneNumber,
            "celPhoneNumber":this.resourceForm.controls.celPhoneNumber
         },
         "email":this.resourceForm.controls.email
      },
      "billingAddress":{
         "address":this.resourceForm.controls.address,
         "number":this.resourceForm.controls.number,
         "complement":this.resourceForm.controls.complement,
         "neighborhood":this.resourceForm.controls.neighborhood,
         "idCity":this.resourceForm.controls.idCity.value,
         "state":this.resourceForm.controls.state,
         "cep":this.resourceForm.controls.cep,
         "country":"BRA"
      },
      "clientType":"CORPORATE",
      "accountType":"UNLIMITED_ORDINARY",
      "additionalDetailsCorporate":{
         "establishmentDate":this.resourceForm.controls.establishmentDate,
         "companyName":this.resourceForm.controls.companyName,
         "representatives":[
            {
               "name": this.resourceForm.controls.represent_name,
               "birthDate":this.resourceForm.controls.represent_birthDate,
               "gender":this.resourceForm.controls.represent_gender,
               "marital_status":this.resourceForm.controls.represent_marital_status,
               "nationality_state":this.resourceForm.controls.represent_nationality_state,
               "nationality":"BRASIL",
               "document_number":this.resourceForm.controls.represent_document_number,
               "document_state":this.resourceForm.controls.represent_document_state,
               "orgao_emissor":this.resourceForm.controls.represent_orgao_emissor,
               "issuance_date":this.resourceForm.controls.represent_issuance_date,
               "taxIdentifier":{
                  "taxId":this.resourceForm.controls.represent_taxId,
                  "country":"BRA"
               },
               "mobilePhone":{
                  "country":"BRA",
                  "phoneNumber":this.resourceForm.controls.represent_phoneNumber
               },
               "email":this.resourceForm.controls.represent_email,
               "mailAddress":{
                  "address": this.resourceForm.controls.represent_address,
                  "number":this.resourceForm.controls.represent_number,
                  "complement":this.resourceForm.controls.represent_complement,
                  "neighborhood":this.resourceForm.controls.represent_neighborhood,
                  "idCity":this.resourceForm.controls.represent_idCity,
                  "state":this.resourceForm.controls.represent_state,
                  "cep":this.resourceForm.controls.represent_cep,
                  "country":"BRA"
               }
            }
         ],
         "controlAccess":{
            "origem":"App"
         }
      }
    }
    console.log(postData);    
   
  }

  alterarDados(){
    this.loadStates();
    this.loadCitys()
  }


  loadStates(){
    this.rest.getStates().subscribe(
        data => {
          var response = JSON.parse(JSON.stringify(data));
          let itens = response.data;
          this.states = [];
          itens.forEach(item => {            
                this.states.push({'value':item.id,'label':item.estado});
          });   
        });
  }

  
  loadCitys(){
    let idUF = this.resourceForm.get('state').value;
       if(idUF =="" || idUF == null){
      this.citys = [];
      return;
    }

    this.rest.getCitys(parseInt(idUF)).subscribe(
        data => {
          var response = JSON.parse(JSON.stringify(data));
          let itens = response.data;
          this.citys = [];
          itens.forEach(item => {            
                this.citys.push({'value':item.id,'label':item.nome_cidade});
          });
      
        });
  }

  loadRepresentCitys(){
    let idUF = this.resourceForm.get('represent_state').value;
       if(idUF =="" || idUF == null){
      this.citys = [];
      return;
    }

    this.rest.getCitys(parseInt(idUF)).subscribe(
        data => {
          var response = JSON.parse(JSON.stringify(data));
          let itens = response.data;
          this.represent_citys = [];
          itens.forEach(item => {            
                this.represent_citys.push({'value':item.id,'label':item.nome_cidade});
          });
      
        });
  }

}
