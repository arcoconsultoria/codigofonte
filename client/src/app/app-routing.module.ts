import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from './guards/auth-guard.service';


const routes: Routes = [
  {
    path: 'veiculo',
    loadChildren: () => import('./pages/veiculo/veiculo.module').then(m => m.VeiculoModule),
   // canActivate: [AuthGuardService]
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then(m => m.LoginModule)
  },
  {
    path:'home',
    loadChildren:()=> import('./pages/home/home.module').then(m=>m.HomeModule),
    canActivate: [AuthGuardService]
  },  
  {
    path:'empresa',
    loadChildren:()=> import('./pages/empresa/empresa.module').then(m=>m.EmpresaModule),
    canActivate: [AuthGuardService]
  }, 
  { path: '', redirectTo: 'home', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
