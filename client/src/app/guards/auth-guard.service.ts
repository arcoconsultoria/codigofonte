import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { ActivatedRoute, Router } from '@angular/router';
import { SessionService } from '../shared/services/session.service';


@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  private isAuthenticated: boolean = false;

  constructor(private session: SessionService,
              private route: ActivatedRoute,
              private router: Router) { }

  canActivate() {
    this.isAuthenticated = this.session.isAuthenticated();
    if(this.isAuthenticated) {
      return true;
    }
    this.router.navigate(['/login/login-form']);
  }

}
