import { SessionService } from './shared/services/session.service';
import { Component } from '@angular/core';
import { Subject } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public logado: Subject<boolean> = this.session.isLogado;
  width:string="10";
  title = 'orcamentacao-client';
  constructor(private session: SessionService, private router: Router,) {
    //this.session.getUserSession();
    this.width = this.logado?"10":"12";
  }


}
